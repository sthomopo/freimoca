# FreiMoCa

**Frei**burg **Mo**nte-**Ca**rlo Analysis Framework

## Setting up the framework

#### First installation

When first cloning the package you will need to install all of the source code. To do so, run:  
```
source compile_all.sh
```

#### Every time you log in

```
source setup.sh [MG/all]
```

## Generating events with MadGraph

MadGraph is installed by the `compile_all.sh` script and can be run standalone without `athena`. 
This can be done by going in the MadGraph directory and running `./bin/mg5_aMC`.

### Time-consuming generation that takes several hours to complete (for very complicated processes)

For very time-consuming generation tasks the best approach is to run MadGraph interactively and let it send jobs to the NAF cluster. This can be done as follows:
- in `input/mg5_configuration.txt` set:
   - `run_mode = 1`
   - `cluster_type = condor`
   - `cluster_size = 1000 `
- in `madgraph/various/cluster.py` inside the `CondorCluster` class add `+RequestRuntime = 140000` after the line `getenv=True` in the `submit` and `submit2`
functions
- generate the process and save the output directory (with the `generate` and `output` commands in the MG prompt)
- save the launch command in a file e.g. `echo "launch /nfs/dust/atlas/user/sargyrop/MC/bbww_QCD3QED2_MG2 aMC@NLO -f -p -c" > command`
- launch MG via `nohup ./bin/mg5_aMC command > output &`
- at periodic intervals (at least once every 24h) the kerberos kredentials need to be renewed using a keytab file. This is done using `kinit -kt keytab.kt.2023
$USER@DESY.DE`. Instructions for generating the keytab file can be found below.

#### Generation of keytab file on NAF

Instructions kindly provided by Karolos Potamianos.

```
export KRB5CCNAME=FILE:$(mktemp)
/usr/bin/ktutil                 
addent -p $USER@DESY.DE -password -k 0 -e aes256-cts-hmac-sha1-96
wkt .keytab.kt.2023
exit
kinit -kt .keytab.kt.2023 $USER@DESY.DE
```

## Generating/Showering MC events in athena

This is done using the scripts in `GenTransforms`. Look at [GenTransforms/README.md](GenTransforms/README.md) for more details.  


## Running a truth-level analysis on EVNT files using RIVET and athena

This is done using the scripts in `RivetInAthena`. Look at [RivetInAthena/README.md](RivetInAthena/README.md) for more details.  


## Standalone showering of LHE events with Pythia

After installing all packages with `source compile_all.sh` the example main functions that are shipped with Pythia can be used to shower
events. This can be done as follows

```
cd pythia8303/examples
make main44
ln -s /path/to/event.lhe .
./main44 main44.cmnd EVNT.hepmc
```

In the 3rd line above a link to an LHE file is made inside the directory where the main functions of Pythia are located. The Pythia run is
steered by `main44.cmnd` so changes to point to the correct input LHE file (and potentially other changes) will be needed before running the
`main44` executable.

The above produces a `HepMC2` formatted file as output. 


## Showering and running RIVET standalone in one go

The above instructions can be extended in order to pass the showered events to a `RIVET` routine. This is done as follows.

```
cd pythia8303/examples
make main44
ln -s /path/to/event.lhe .
mkfifo hepmc.fifo
./main44 main44.cmnd hepmc.fifo &
rivet -a MC_TTBAR hepmc.fifo
```

This will produce a `YODA` file as output which can then be processed with e.g.
```
rivet-mkhtml Rivet.yoda
```

or converted to a `ROOT` file via `yoda2root`.
