if [[ "$#" != "1" || ("$1" != "MG" && "$1" != "all") ]] ; then 
  echo "You need to speficy whether to setup MG only or everything"
  echo "source setup.sh MG/all"
  return 
fi

export BASEDIR=$PWD

# Set up LHAPDF
export PATH=$PATH:$BASEDIR/LHAPDF-6.3.0/gcc620_x86_64-slc6/bin

if [[ "$1" == "MG" ]] ; then return ; fi

# Set up gcc
setupATLAS
lsetup "gcc gcc620_x86_64_slc6" 

# Set up rivet
source $BASEDIR/Rivet-3.1.0/rivetenv.sh

# Set up yoda
source $BASEDIR/YODA-1.8.0/yodaenv.sh

# Set up root
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
