#!/bin/bash

###################
# Global settings #
###################
jOFilePath=$1
inputFilePath=$2
release=$3
seed=$4
DESTINATION=$5

# Check if the correct arguments have been provided
if [ "$#" != 5 ] ; then
  echo "Usage: ./runTransform.sh JOPATH INPUTFILE RELEASE SEED"
  exit 1
else 
  if [ ! -f "$jOFilePath" ] ; then
    echo "jO: $jOFilePath does not exist"
    exit 2
  fi
  if [ ! -f "$inputFilePath" ] ; then 
    echo "Input file: $inputFilePath does not exist"
    exit 3  
  fi
  if [ ! -d $DESTINATION ] ; then
    echo "Output destination: $DESTINATION does not exist - please create directory"
    exit 4
  fi
fi

# Extract the names of the jO and input files
jO=$(echo $jOFilePath | awk -F '/' '{print $NF}')
inputFile=$(echo $inputFilePath | awk -F '/' '{print $NF}')

# Form the name of the output file
tag=$(echo $inputFile | awk -F '.' '{printf "%s%s" , $2, $3}')
outputFile=$(echo $inputFile | awk -F '.' '{printf "EVNT.%s%s.pool.root" , $2, $3}')

######################
# set up environment #
######################

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# Set up athena release
asetup $release

# Make a temporary directory where everything will be stored
TMPDIR=$(mktemp -d) && cd $TMPDIR

# Make a run directory inside the TMPDIR where athena will run
# It produces a lot of junk files that we don't want to store
RUNDIR=$TMPDIR/run && rm -rf $RUNDIR && mkdir $RUNDIR

# Copy the jO and input files to TMPDIR
cp $jOFilePath $TMPDIR
cp $inputFilePath $TMPDIR

# Check if they have been copied
if [ ! -f "$TMPDIR/$jO" ] ; then
  echo "ERROR while copying $jO"
  exit 5
fi
if [ ! -f "$TMPDIR/$inputFile" ] ; then
  echo "ERROR while copying $inputFile"
  exit 6
fi

#####################
# Run the transform #
#####################

# First need to go in the run directory
cd $RUNDIR

# Call Gen_tf
Gen_tf.py --jobConfig ../ --ecmEnergy=13000. \
--outputEVNTFile=$outputFile \
--randomSeed=$seed \
--inputGeneratorFile=$TMPDIR/$inputFile

# Check if the output file has been produced
if [ ! -f $outputFile ] ; then
  echo "ERROR producing $outputFile"
  exit 7
fi

# Copy output file and log.generate to specified location
cp $outputFile $DESTINATION
cp log.generate $DESTINATION/log.generate'_'$tag

# Clean up 
rm -rf $TMPDIR

exit 0
