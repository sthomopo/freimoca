# GenTransforms

A package that contains a set of scripts that set up and run the ATLAS MC software (`Gen_tf.py`).  
The package contains:  
*  Scripts that run the generation transform (under `scripts/`)
*  Various jobOptions that steer what the transform does (under `jO/`)


### How to run interactively

```
./scripts/runTransform.sh JOFILE INPUT_FILE RELEASE SEED DESTINATION
```

for example  

```
./scripts/runTransform.sh $PWD/jO/mc.PhH7EG_AU2CT10_WlnuWlnu.py  /work/ws/atlas/sa1083-MCSamples/mc15_13TeV.361591.PhHppEG_AU2CT10_WlnuWlnu.evgen.TXT.e4696/TXT.19990447._002207.tar.gz.1 AthGeneration,21.6.17 123456 $PWD/output
```
