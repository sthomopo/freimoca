#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/MissingMomentum.hh"

// For smearing
#include "Rivet/Projections/SmearedJets.hh"
#include "Rivet/Projections/SmearedParticles.hh"
#include "Rivet/Projections/SmearedMET.hh"


namespace Rivet {


  /// AZH -> ll ttbar analysis
  /// Author: Spyros Argyropoulos <spyridon.argyropoulos@cern.ch>
  /// Based on ttbar l+jets routine: https://rivet.hepforge.org/analyses/ATLAS_2018_I1656578.html
  
  class AZH_lltt : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(AZH_lltt);


    /// Book cuts and projections
    void init() {
    
      if ( getOption("SMEAR") == "ON" ) m_smear = true; // default is false - to turn ann use 
    
      // Cuts
      Cut eta_full       = (Cuts::abseta < 5.0);
      Cut lep_cuts       = (Cuts::abseta < 2.5) && (Cuts::pT > 25*GeV);
      Cut lep_cuts_loose = (Cuts::abseta < 3.0) && (Cuts::pT > 5*GeV);

      // All final state particles
      FinalState fs(eta_full);

      // Get photons to dress leptons
      IdentifiedFinalState all_photons(fs);
      all_photons.acceptIdPair(PID::PHOTON);

      PromptFinalState photons(Cuts::abspid == PID::PHOTON, true);
      declare(photons, "photons");

      // Projection to find the electrons
      PromptFinalState electrons(Cuts::abspid == PID::ELECTRON, true);

      DressedLeptons dressedelectrons(photons, electrons, 0.1, lep_cuts);
      DressedLeptons dressedelectrons_loose(photons, electrons, 0.1, lep_cuts_loose);
      declare(dressedelectrons, "elecs");
      declare(dressedelectrons_loose, "elecs_loose");

      DressedLeptons ewdressedelectrons(all_photons, electrons, 0.1, eta_full);

      // Projection to find the muons
      PromptFinalState muons(Cuts::abspid == PID::MUON, true);

      DressedLeptons dressedmuons(photons, muons, 0.1, lep_cuts);
      DressedLeptons dressedmuons_loose(photons, muons, 0.1, lep_cuts_loose);
      declare(dressedmuons, "muons");
      declare(dressedmuons_loose, "muons_loose");

      DressedLeptons ewdressedmuons(all_photons, muons, 0.1, eta_full);

      // Projection to find MET
      declare(MissingMomentum(fs), "MET");
           
      // Jet clustering.
      VetoedFinalState vfs(fs);
      vfs.addVetoOnThisFinalState(ewdressedelectrons);
      vfs.addVetoOnThisFinalState(ewdressedmuons);
      FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      declare(jets, "jets");
            
      // --- Smeared objects --- //
      
      // Smeared Jets
      declare(SmearedJets(jets, JET_SMEAR_ATLAS_RUN2, JET_BTAG_ATLAS_RUN2_MV2C10), "s_jets");

      // Projection for smeared MET
      declare(SmearedMET(MissingMomentum(fs), MET_SMEAR_ATLAS_RUN2), "s_MET");
      
      // Electrons and muons
      // Note that ideally one would want to veto the smeared leptons from the smeared jets final state
      // but the difference should not be huge
      declare(SmearedParticles(dressedelectrons, ELECTRON_RECOEFF_ATLAS_RUN2, ELECTRON_SMEAR_ATLAS_RUN2), "s_elecs");
      declare(SmearedParticles(dressedmuons, MUON_EFF_ATLAS_RUN2, MUON_SMEAR_ATLAS_RUN2), "s_muons");
      declare(SmearedParticles(dressedelectrons_loose, ELECTRON_RECOEFF_ATLAS_RUN2, ELECTRON_SMEAR_ATLAS_RUN2), "s_elecs_loose");
      declare(SmearedParticles(dressedmuons_loose, MUON_EFF_ATLAS_RUN2, MUON_SMEAR_ATLAS_RUN2), "s_muons_loose");
      
      // --- Histograms --- //
      
      // Inclusive histograms
      book(_h["allEvents"],    "allEvents",    1, 0., 2.);
      book(_h["njets_inc"],    "njets_inc",    10, -0.5, 9.5);
      book(_h["nbjets_inc"],   "nbjets_inc",   6, -0.5, 5.5);
      book(_h["met_inc"],      "met_inc",      250, 0, 500);
      book(_h["nleptons_inc"], "nleptons_inc", 5, -0.5, 4.5);
      book(_h["lepton1_pt"],   "lepton1_pt",   150, 0., 1500.);
      book(_h["lepton2_pt"],   "lepton2_pt",   100, 0., 1000.);
      book(_h["lepton3_pt"],   "lepton3_pt",   50, 0., 500.);
      book(_h["lepton1_eta"],  "lepton1_eta",  70, -3.5, 3.5);
      book(_h["lepton2_eta"],  "lepton2_eta",  70, -3.5, 3.5);
      book(_h["lepton3_eta"],  "lepton3_eta",  70, -3.5, 3.5);
      book(_h["ljet1_pt"],     "ljet1_pt",     150, 0., 1500.);
      book(_h["ljet2_pt"],     "ljet2_pt",     100, 0., 1000.);
      book(_h["ljet1_eta"],    "ljet1_eta",    80, -4.0, 4.0);
      book(_h["ljet2_eta"],    "ljet2_eta",    80, -4.0, 4.0);
      book(_h["bjet1_pt"],     "bjet1_pt",     150, 0., 1500.);
      book(_h["bjet2_pt"],     "bjet2_pt",     100, 0., 1000.);
      book(_h["bjet1_eta"],    "bjet1_eta",    80, -4.0, 4.0);
      book(_h["bjet2_eta"],    "bjet2_eta",    80, -4.0, 4.0);
      
      // Histograms after event selection
      book(_h["njets_sel"],    "njets_sel",    10, -0.5, 9.5);
      book(_h["nbjets_sel"],   "nbjets_sel",   6, -0.5, 5.5);
      book(_h["met_sel"],      "met_sel",      200, 0, 1000);
      book(_h["nleptons_sel"], "nleptons_sel", 5, -0.5, 4.5);
      book(_h["m_ptoplep"],    "m_ptoplep",    160, 0, 800);
      book(_h["m_ptophad"],    "m_ptophad",    160, 0, 800);
      book(_h["dR_lep_bjet0"], "dR_lep_bjet0", 50, 0, 5);
      book(_h["dR_lep_bjet1"], "dR_lep_bjet1", 50, 0, 5);
      book(_h["dR_bb"],        "dR_bb",        50, 0, 5);
      book(_h["dPhi_MET_Z"],   "dPhi_MET_Z",   64, 0, 3.2);
      book(_h["dPhi_MET_lep"], "dPhi_MET_lep", 64, 0, 3.2);     
      book(_h["m_Whad"],       "m_Whad",       150, 0, 300);
      book(_h["m_Z"],          "m_Z",          150, 0, 300);      
      book(_h["m_ttbar"],      "m_ttbar",      600, 0, 3000);
      book(_h["m_Zttbar"],     "m_Zttbar",     800, 0, 4000);
 
    }


    void analyze(const Event& event) {

      // Get the selected objects, using the projections.
      const Particles& electrons = (!m_smear) ? apply<DressedLeptons>(event, "elecs").particlesByPt() :
      					        apply<ParticleFinder>(event, "s_elecs").particlesByPt();
      const Particles& muons     = (!m_smear) ? apply<DressedLeptons>(event, "muons").particlesByPt() :
      						apply<ParticleFinder>(event, "s_muons").particlesByPt();
      const Particles& electrons_loose = (!m_smear) ? apply<DressedLeptons>(event, "elecs_loose").particlesByPt() :
      					              apply<ParticleFinder>(event, "s_elecs_loose").particlesByPt();
      const Particles& muons_loose     = (!m_smear) ? apply<DressedLeptons>(event, "muons_loose").particlesByPt() :
      						      apply<ParticleFinder>(event, "s_muons_loose").particlesByPt();
      const Jets& jets = (!m_smear) ? apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5) :
      				      apply<JetAlg>(event, "s_jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5);
      const Jets& jets_loose = (!m_smear) ? apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 10*GeV && Cuts::abseta < 3.5) :
      				            apply<JetAlg>(event, "s_jets").jetsByPt(Cuts::pT > 10*GeV && Cuts::abseta < 3.5);
      const Vector3 met = (!m_smear) ? apply<MissingMomentum>(event, "MET").vectorMPT() :
      				       apply<SmearedMET>(event, "s_MET").vectorMPT();

      Jets bjets, lightjets, bjets_loose, lightjets_loose;
      for (Jet jet : jets) {
        bool b_tagged = jet.bTagged(Cuts::pT > 5*GeV);
        if ( b_tagged ) bjets += jet;
        else lightjets += jet;
      }
      for (Jet jet : jets_loose) {
        bool b_tagged = jet.bTagged(Cuts::pT > 5*GeV);
        if ( b_tagged ) bjets_loose += jet;
        else lightjets_loose += jet;
      }
      
      // Leptons
      // TODO: check impact of 3e/3mu channel
      bool has3leptons = (electrons.size()==2 & muons.size()==1) | (electrons.size()==1 & muons.size()==2);
      const Particle *lepton = NULL;
      if (has3leptons && electrons.size()==1)  lepton = &electrons[0];
      else if (has3leptons && muons.size()==1) lepton = &muons[0];
      
      int njets  = jets.size();
      int nbjets = bjets.size();
      int nljets = lightjets.size();
      
      // Fill inclusive histograms
      _h["allEvents"]->fill(1.);
      _h["njets_inc"]->fill(njets);
      _h["nbjets_inc"]->fill(nbjets);
      _h["met_inc"]->fill(met.perp()*GeV);
      _h["nleptons_inc"]->fill(electrons.size()+muons.size());
      
      // Find leading leptons and fill histograms
      vector<pair<double,double>> leading_lepton_pt_eta;
      for (auto el : electrons_loose) leading_lepton_pt_eta.push_back({el.pt(), el.eta()});
      for (auto mu: muons_loose) leading_lepton_pt_eta.push_back({mu.pt(), mu.eta()});
      sort(leading_lepton_pt_eta.rbegin(), leading_lepton_pt_eta.rend());
      if (leading_lepton_pt_eta.size() > 0) {
        _h["lepton1_pt"]->fill(leading_lepton_pt_eta.at(0).first*GeV);
	_h["lepton1_eta"]->fill(leading_lepton_pt_eta.at(0).second);
      }
      if (leading_lepton_pt_eta.size() > 1) {
        _h["lepton2_pt"]->fill(leading_lepton_pt_eta.at(1).first*GeV);
	_h["lepton2_eta"]->fill(leading_lepton_pt_eta.at(1).second);
      }
      if (leading_lepton_pt_eta.size() > 2) {
        _h["lepton3_pt"]->fill(leading_lepton_pt_eta.at(2).first*GeV);
	_h["lepton3_eta"]->fill(leading_lepton_pt_eta.at(2).second);
      }
      
      // Fill histograms for leading jets and b-jets
      if (lightjets_loose.size() > 0) {
        _h["ljet1_pt"]->fill(lightjets_loose.at(0).pt()*GeV);
	_h["ljet1_eta"]->fill(lightjets_loose.at(0).eta());
      }
      if (lightjets_loose.size() > 1) {
        _h["ljet2_pt"]->fill(lightjets_loose.at(1).pt()*GeV);
	_h["ljet2_eta"]->fill(lightjets_loose.at(1).eta());
      }
      if (bjets_loose.size() > 0) {
        _h["bjet1_pt"]->fill(bjets_loose.at(0).pt()*GeV);
	_h["bjet1_eta"]->fill(bjets_loose.at(0).eta());
      }
      if (bjets_loose.size() > 1) {
        _h["bjet2_pt"]->fill(bjets_loose.at(1).pt()*GeV);
	_h["bjet2_eta"]->fill(bjets_loose.at(1).eta());
      }
      
      // Pre-selection
      if ( !has3leptons ) vetoEvent;
      if ( njets  < 4   ) vetoEvent;
      if ( nbjets < 2   ) vetoEvent;
      if ( nljets < 2   ) vetoEvent;
      
      // Find leptons that constitute Z candidate
      FourMomentum Zboson;
      double mZPDG = 91.188*GeV;
      if (electrons.size() == 2) Zboson = electrons[0].momentum() + electrons[1].momentum();
      else Zboson = muons[0].momentum() + muons[1].momentum(); 

      FourMomentum pbjet1; //Momentum of bjet1
      FourMomentum pbjet2; //Momentum of bjet2

      if ( deltaR(bjets[0], *lepton) <= deltaR(bjets[1], *lepton) ) {
        pbjet1 = bjets[0].momentum();
        pbjet2 = bjets[1].momentum();
      } else {
        pbjet1 = bjets[1].momentum();
        pbjet2 = bjets[0].momentum();
      }
            
      // Find jets that consitute hadronic W candidate
      double bestWmass = 1000.0*TeV;
      double mWPDG = 80.399*GeV;
      int Wj1index = -1, Wj2index = -1;
      for (unsigned int i = 0; i < (lightjets.size() - 1); ++i) {
        for (unsigned int j = i + 1; j < lightjets.size(); ++j) {
          double wmass = (lightjets[i].momentum() + lightjets[j].momentum()).mass();
          if (fabs(wmass - mWPDG) < fabs(bestWmass - mWPDG)) {
            bestWmass = wmass;
            Wj1index = i;
            Wj2index = j;
          }
        }
      }

      FourMomentum pjet1 = lightjets[Wj1index].momentum();
      FourMomentum pjet2 = lightjets[Wj2index].momentum();

      // compute hadronic W boson
      FourMomentum pWhadron = pjet1 + pjet2;
      double pz = computeneutrinoz(lepton->momentum(), met);
      FourMomentum ppseudoneutrino( sqrt(sqr(met.x()) + sqr(met.y()) + sqr(pz)), met.x(), met.y(), pz);

      //compute leptonic, hadronic, combined pseudo-top
      FourMomentum ppseudotoplepton = lepton->momentum() + ppseudoneutrino + pbjet1;
      FourMomentum ppseudotophadron = pbjet2 + pWhadron;
      FourMomentum pttbar = ppseudotoplepton + ppseudotophadron;
            
      // ttbar mass
      double m_ttbar = pttbar.mass();
      
      // Ztt mass
      double m_Zttbar = (pttbar + Zboson).mass();

      // Fill histograms after selection
      _h["njets_sel"]->fill(njets);
      _h["nbjets_sel"]->fill(nbjets);
      _h["met_sel"]->fill(met.perp()*GeV);
      _h["nleptons_sel"]->fill(electrons.size()+muons.size());
      _h["m_ptoplep"]->fill(ppseudotoplepton.mass()*GeV);
      _h["m_ptophad"]->fill(ppseudotophadron.mass()*GeV);
      _h["m_ttbar"]->fill(m_ttbar*GeV);
      _h["m_Zttbar"]->fill(m_Zttbar*GeV);
      _h["dR_lep_bjet0"]->fill(deltaR(bjets[0], *lepton));
      _h["dR_lep_bjet1"]->fill(deltaR(bjets[1], *lepton));
      _h["dR_bb"]->fill(deltaR(bjets[0], (bjets[1])));
      _h["dPhi_MET_Z"]->fill(deltaPhi(met, Zboson));
      _h["dPhi_MET_lep"]->fill(deltaPhi(met, *lepton));
      _h["m_Whad"]->fill(pWhadron.mass()*GeV);
      _h["m_Z"]->fill(Zboson.mass()*GeV);
    }


    void finalize() {
      /*
      // Normalize to cross-section
      const double sf = (crossSection() / sumOfWeights());
      for (auto hist : _h) {
        scale(hist.second, sf);
        // Normalized distributions
        if (hist.first.find("_norm") != string::npos)  normalize(hist.second);
      }
      */
    }


    double computeneutrinoz(const FourMomentum& lepton, const Vector3 &met) const {
      // computing z component of neutrino momentum given lepton and met
      double pzneutrino;
      double m_W = 80.399; // in GeV, given in the paper
      double k = (( sqr( m_W ) - sqr( lepton.mass() ) ) / 2 ) + (lepton.px() * met.x() + lepton.py() * met.y());
      double a = sqr ( lepton.E() )- sqr ( lepton.pz() );
      double b = -2*k*lepton.pz();
      double c = sqr( lepton.E() ) * sqr( met.mod() ) - sqr( k );
      double discriminant = sqr(b) - 4 * a * c;
      double quad[2] = { (- b - sqrt(discriminant)) / (2 * a), (- b + sqrt(discriminant)) / (2 * a) }; //two possible quadratic solns
      if (discriminant < 0)  pzneutrino = - b / (2 * a); //if the discriminant is negative
      else { //if the discriminant is greater than or equal to zero, take the soln with smallest absolute value
        double absquad[2];
        for (int n=0; n<2; ++n)  absquad[n] = fabs(quad[n]);
        if (absquad[0] < absquad[1])  pzneutrino = quad[0];
        else                          pzneutrino = quad[1];
      }
      return pzneutrino;
    }


  private:

    /// @name Objects that are used by the event selection decisions
    map<string, Histo1DPtr> _h;
    
    bool m_smear = false;

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(AZH_lltt);


}
