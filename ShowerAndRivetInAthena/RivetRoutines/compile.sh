#!/bin/bash

if [ "$#" != 1 ] ; then
  echo "Usage: ./compile.sh /path/to/RivetAnalysis.cc"
  exit 1
fi

INPUTFILEPATH=$1

if [ ! -f "$INPUTFILEPATH" ]; then
  echo "ERROR: $INPUTFILEPATH does not exist"
  exit 2
fi

###################
# Global settings #
###################
release=AthGeneration,21.6.33

######################
# set up environment #
######################

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# Set up athena release
asetup $release

# Set up rivet
. setupRivet.sh

######################
# Build              #
######################

# Move to directory where input is located
cd $(dirname $INPUTFILEPATH)

# Extract name of source file
INPUTFILENAME=$(echo $INPUTFILEPATH | awk -F '/' '{print $NF}')
BASENAME=$(echo $INPUTFILENAME | awk -F '.' '{print $1}')

# build
rivet-build Rivet$BASENAME.so $INPUTFILENAME

exit 0
