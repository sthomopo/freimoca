#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"

// For smearing
#include "Rivet/Projections/SmearedJets.hh"
#include "Rivet/Projections/SmearedParticles.hh"
#include "Rivet/Projections/SmearedMET.hh"

#include <cstdio>
#include <math.h>

namespace Rivet {


  /// AZH -> vv bbbar analysis
  /// Author: Spyros Argyropoulos <spyridon.argyropoulos@cern.ch>
  
  class AZH_vvbb : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(AZH_vvbb);


    /// Book cuts and projections
    void init() {
    
      if ( getOption("SMEAR") == "ON" ) m_smear = true; // default is false - to turn ann use 
      m_sample = getOption("SAMPLE");
    
      // Cuts
      Cut eta_full       = (Cuts::abseta < 5.0);
      Cut lep_cuts       = (Cuts::abseta < 2.5) && (Cuts::pT > 7*GeV);

      // All final state particles
      FinalState fs(eta_full);

      // Get photons to dress leptons
      IdentifiedFinalState all_photons(fs);
      all_photons.acceptIdPair(PID::PHOTON);

      PromptFinalState photons(Cuts::abspid == PID::PHOTON, true);
      declare(photons, "photons");

      // Projection to find the electrons
      PromptFinalState electrons(Cuts::abspid == PID::ELECTRON, true);

      DressedLeptons dressedelectrons(photons, electrons, 0.1, lep_cuts);
      declare(dressedelectrons, "elecs");

      DressedLeptons ewdressedelectrons(all_photons, electrons, 0.1, eta_full);

      // Projection to find the muons
      PromptFinalState muons(Cuts::abspid == PID::MUON, true);

      DressedLeptons dressedmuons(photons, muons, 0.1, lep_cuts);
      declare(dressedmuons, "muons");

      DressedLeptons ewdressedmuons(all_photons, muons, 0.1, eta_full);

      // Projection to find MET
      MissingMomentum missmom(fs);
      declare(missmom, "MET");
           
      // Jet clustering.
      VetoedFinalState vfs(fs);
      vfs.addVetoOnThisFinalState(ewdressedelectrons);
      vfs.addVetoOnThisFinalState(ewdressedmuons);
      
      // small-R jets
      FastJets jets(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      declare(jets, "jets");
      
      // track-jets
      ChargedFinalState cfs(Cuts::pT > 0.5*GeV && Cuts::abseta < 2.5);
      FastJets trackjets(cfs, FastJets::ANTIKT, 0.2, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      declare(trackjets, "trackjets");
      
      // large-R jets
      FastJets fatjets(vfs, FastJets::ANTIKT, 1.0, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      declare(fatjets, "fatjets");

            
      // --- Smeared objects --- //
      
      // Smeared Jets
      declare(SmearedJets(jets, JET_SMEAR_ATLAS_RUN2, JET_BTAG_ATLAS_RUN2_MV2C10), "s_jets");

      // Projection for smeared MET
      declare(SmearedMET(MissingMomentum(fs), MET_SMEAR_ATLAS_RUN2), "s_MET");
      
      // Electrons and muons
      // Note that ideally one would want to veto the smeared leptons from the smeared jets final state
      // but the difference should not be huge
      declare(SmearedParticles(dressedelectrons, ELECTRON_RECOEFF_ATLAS_RUN2, ELECTRON_SMEAR_ATLAS_RUN2), "s_elecs");
      declare(SmearedParticles(dressedmuons, MUON_EFF_ATLAS_RUN2, MUON_SMEAR_ATLAS_RUN2), "s_muons");
      
      // --- Histograms --- //
      
      // Inclusive histograms
      book(_h["allEvents"],         "allEvents",    1, 0., 2.);
      book(_h["njets_inc"],         "njets_inc",    10, -0.5, 9.5);
      book(_h["met_inc"],           "met_inc",      200, 0, 1000);
      book(_h["met_inc_afterTrig"], "met_inc_afterTrig",      200, 0, 1000);
      book(_h["nleptons_inc"],      "nleptons_inc", 5, -0.5, 4.5);

      // Histograms after event selection
      book(_h["njets"],            "njets",           10, -0.5, 9.5);
      book(_h["nbjets"],           "nbjets",          6, -0.5, 5.5);
      book(_h["nfatjets"],         "nfatjets",        6, -0.5, 5.5);
      book(_h["ntrackjet"],        "ntrackjet",       6, -0.5, 5.5);
      book(_h["nbtagTJ"],          "nbtagTJ",         6, -0.5, 5.5);
      book(_h["met"],              "met",             200, 0, 1000);
      book(_h["met_afterTrig"],    "met_afterTrig",   200, 0, 1000);
      book(_h["nleptons"],         "nleptons",        5, -0.5, 4.5);
      book(_h["pmag_bfromH"],      "pmag_bfromH",     200, 0., 2000.);
      book(_h["pmag_BfromH"],      "pmag_BfromH",     200, 0., 2000.);
      book(_h["mH_firstBquarks"],  "mH_firstBquarks", 200, 0., 2000.);
      book(_h["mH_lastBquarks"],   "mH_lastBquarks",  200, 0., 2000.);
      book(_h["mH_firstBhadrons"], "mH_firstBhadrons",200, 0., 2000.);
      book(_h["mH_lastBhadrons"],  "mH_lastBhadrons", 200, 0., 2000.);
      book(_h["mLeadFatJet"],      "mLeadFatJet",     200, 0., 2000.);
      book(_h["dR_firstBquarks"],  "dR_firstBquarks", 50,  0., 5.);
      book(_h["dR_b1b2"],          "dR_b1b2",         50,  0., 5.);
      book(_h["pt_ACand"],         "pt_ACand",        100,  0., 1000.);
      book(_h["pt_Atruth"],        "pt_Atruth",       100,  0., 1000.);
      book(_h["pt_HCand"],         "pt_HCand",        100,  0., 1000.);
      book(_h["pt_b1"],            "pt_b1",           100,  0., 1000.);
      book(_h["pt_b2"],            "pt_b2",           50 ,  0., 500.);
      book(_h["pt_j1"],            "pt_j1",           100,  0., 1000.);
      book(_h["m_HCand"],          "m_HCand",         200,  0., 2000.);
      book(_h["mT_ACand"],         "mT_ACand",        300,  0., 3000.);
      book(_h["dPhi_MetH"],        "dPhi_MetH",       32,   0., 3.2);
      book(_h["dR_MetH"],          "dR_MetH",         50,   0., 5.0);
      book(_h["dPhi_MetZ"],        "dPhi_MetZ",       32,   0., 3.2);
      book(_h["dR_MetZ"],          "dR_MetZ",         50,   0., 5.0);
      book(_h["mtop_closest"],     "mtop_closest",    100,  0., 500.);
      book(_h["mtop_furthest"],    "mtop_furthest",   100,  0., 500.);
      book(_h["mtop_average"],     "mtop_average",    100,  0., 500.);
      book(_h["metSig"],           "metSig",          50,  0., 50.);
      // SR plots      
      book(_h["mtop_average_SR"],  "mtop_average_SR", 100,  0., 500.);
      book(_h["m_HCand_SR"],       "m_HCand_SR",      200,  0., 2000.);
      book(_h["mT_ACand_SR"],      "mT_ACand_SR",     300,  0., 3000.);
      book(_h["dPhi_MetH_SR"],     "dPhi_MetH_SR",    32,   0., 3.2);
      book(_h["njets_SR"],         "njets_SR",        10, -0.5, 9.5);
      
      std::string filename = m_sample+".csv";
      fp = fopen(filename.c_str(), "w");
      fprintf(fp, "#sample , weight*1000 , njets , met_x , met_y , met_z , met_e , b1_x , b1_y , b1_z , b1_e , b2_x , b2_y , b2_z , b2_e , mTA , passSRcuts\n");
    }


    void analyze(const Event& event) {

      // Get the selected objects, using the projections.
      const Particles& electrons = (!m_smear) ? apply<DressedLeptons>(event, "elecs").particlesByPt() :
      					        apply<ParticleFinder>(event, "s_elecs").particlesByPt();
      const Particles& muons     = (!m_smear) ? apply<DressedLeptons>(event, "muons").particlesByPt() :
      						apply<ParticleFinder>(event, "s_muons").particlesByPt();
      const Jets& jets = (!m_smear) ? apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5) :
      				      apply<JetAlg>(event, "s_jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5);
      const Vector3 met = (!m_smear) ? apply<MissingMomentum>(event, "MET").vectorMPT() :
      				       apply<SmearedMET>(event, "s_MET").vectorMPT();
 
      
      /* -------------------------*/
      /*       Large-R jets       */
      /* -------------------------*/      

      const PseudoJets& fatjets = apply<FastJets>(event, "fatjets").pseudoJetsByPt(200*GeV);
      
      // Trimming
      fastjet::Filter trimmer(fastjet::JetDefinition(fastjet::kt_algorithm, 0.2), fastjet::SelectorPtFractionMin(0.05));
      Jets trimmedFatJets;
      for (const Jet& jet : fatjets) {
        Jet trimmedJet = trimmer(jet);
	if (trimmedJet.pT() > 200*GeV && fabs(trimmedJet.eta()) < 2.0) trimmedFatJets += Jet(trimmer(jet));
      }
      std::sort(trimmedFatJets.begin(), trimmedFatJets.end(), cmpMomByPt);
      
      // Retrieve trackjets
      const Jets& trackjets = apply<FastJets>(event, "trackjets").jetsByPt(Cuts::pT > 10*GeV);
      
      // Ghost-associate track-jets to fat-jets
      PseudoJets pjs;
      // Constituents of the trimmed fatjet
      if (trimmedFatJets.size() > 0) for (auto tj : trimmedFatJets.at(0).pseudojet().constituents()) {
        fastjet::PseudoJet pj = tj;
        pj.set_user_index(-1); // dummmy
        pjs.push_back(pj);
      }
      // Ghostify trackjets
      for (size_t i = 0; i < trackjets.size(); ++i) {
        fastjet::PseudoJet pj = trackjets[i];
        pj *= 1e-20; // ghostify momentum
        pj.set_user_index(i);
        pjs.push_back(pj);
      }
      
      // Find track-jets ghost-associated to leading fatjet
      fastjet::ClusterSequence tagged_seq(pjs, fastjet::JetDefinition(fastjet::antikt_algorithm, 1.0));
      PseudoJets GAfatjet = fastjet::sorted_by_pt(tagged_seq.inclusive_jets(50.0));
      Jets associated_trackjets;
      if (GAfatjet.size() > 0) for (auto pj : GAfatjet.at(0).constituents()) {
        if (pj.user_index() >= 0)  associated_trackjets += trackjets[pj.user_index()];
      }
      std::sort(associated_trackjets.begin(), associated_trackjets.end(), cmpMomByPt);
      
      // GA b-tagged track-jets
      int nbjets_GATJ = 0;
      if (associated_trackjets.size() > 1) for (unsigned int ij = 0; ij < 2; ++ij) {
        if (associated_trackjets.at(ij).bTagged(Cuts::pT > 5*GeV)) ++nbjets_GATJ;
      }
           
      Jets bjets, ljets;
      double HT = 0.0;
      for (Jet jet : jets) {
        HT += jet.pT();
        bool b_tagged = jet.bTagged(Cuts::pT > 5*GeV);
        if ( b_tagged ) bjets += jet;
	else ljets += jet;
      }
      
      /* -------------------------*/
      /*      Truth particles     */
      /* -------------------------*/
      bool isSignalSample = (m_sample.find("AZH") != std::string::npos);
      
      Particle Aboson, Higgs, Zboson;
      Particles neutrinos;
      if (isSignalSample) {
        findLastParticleById(Aboson, event, 36);
        findLastParticleById(Higgs,  event, 35);
	findLastParticleById(Zboson, event, 23);
	findNeutrinos(neutrinos, event);
      }      
            
      // Find the b-quarks from the Higgs
      Particles firstBQuarksFromHiggs;
      if (isSignalSample) findFirstParticlesFromDecay(Higgs, firstBQuarksFromHiggs, PID::BQUARK);
      double bfromH_maxp = (isSignalSample) ? std::max(firstBQuarksFromHiggs.at(0).p(), firstBQuarksFromHiggs.at(0).p()) : -1;
      
      // Find the last b-quarks in the Higgs decay chain
      Particles lastBQuarksFromHiggs;
      if (isSignalSample) findLastParticlesFromDecay(Higgs, lastBQuarksFromHiggs, PID::BQUARK);
      
      // Find the last b-hadrons in the Higgs decay chain
      Particles lastBHadronsFromHiggs;
      if (isSignalSample) {
        findLastBHadronsFromDecay(firstBQuarksFromHiggs.at(0), lastBHadronsFromHiggs);   
        findLastBHadronsFromDecay(firstBQuarksFromHiggs.at(1), lastBHadronsFromHiggs);  
      }
      
      // Find the first b-hadrons from the b-quarks from the Higgs decay
      Particles firstBHadronsFromHiggs;
      if (isSignalSample) {
        findFirstBHadronsFromBquarks(lastBQuarksFromHiggs.at(0), firstBHadronsFromHiggs);
        findFirstBHadronsFromBquarks(lastBQuarksFromHiggs.at(1), firstBHadronsFromHiggs);
      }
      double BfromH_maxp = (isSignalSample) ? std::max(firstBHadronsFromHiggs.at(0).p(), firstBHadronsFromHiggs.at(0).p()) : -1;
            
      double mH_firstBquarks  = (isSignalSample) ? (firstBQuarksFromHiggs.at(0).momentum()+firstBQuarksFromHiggs.at(1).momentum()).mass()*GeV : -1;
      double mH_lastBquarks   = (isSignalSample) ? (lastBQuarksFromHiggs.at(0).momentum()+lastBQuarksFromHiggs.at(1).momentum()).mass()*GeV : -1;
      double mH_firstBhadrons = (isSignalSample) ? (firstBHadronsFromHiggs.at(0).momentum()+firstBHadronsFromHiggs.at(1).momentum()).mass()*GeV : -1;
      double mH_lastBhadrons  = (isSignalSample) ? (lastBHadronsFromHiggs.at(0).momentum()+lastBHadronsFromHiggs.at(1).momentum()).mass()*GeV : -1;

      //printf("  -> Higgs mH = %3.1f , bb = %3.1f , bb_last = %3.1f , BB_last = %3.1f\n",
      //mH, mH_firstBquarks, mH_lastBquarks, mH_lastBhadrons);
            

      /* -------------------------*/
      /*   Trigger simulation     */
      /* -------------------------*/
      double trigEff = 0.5*(1.+erf((met.perp()*GeV-100.)/(std::sqrt(2.)*60.)));
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_real_distribution<> dis(0, 1);
      double ran = dis(gen);
      bool passTrig = (m_smear) ? (ran < trigEff) : 1;
      //printf("MET = %5.1f , eff = %3.2f , ran = %3.2f , pass = %1d\n", met.perp()*GeV, trigEff, ran, passTrig);
 
      
      /* -------------------------*/
      /*   Objects & Selection    */
      /* -------------------------*/
      bool   has0leptons = (electrons.size()==0) & (muons.size()==0);     
      int    njets       = jets.size();
      int    nbjets      = bjets.size();
      int    nTrimmedFat = trimmedFatJets.size();
                        
      // Fill inclusive histograms
      _h["allEvents"]->fill(1.);
      _h["njets_inc"]->fill(njets);
      _h["met_inc"]->fill(met.perp()*GeV);
      if (passTrig) _h["met_inc_afterTrig"]->fill(met.perp()*GeV);
      _h["nleptons_inc"]->fill(electrons.size()+muons.size());
      
      ++m_nEvents_processed;
      m_sumWeights_processed += event.weights()[0];
              
      // Pre-selection
      if ( !has0leptons )              vetoEvent;
      if ( njets  < 2   )              vetoEvent;
      if ( nbjets != 2  )              vetoEvent;
      if ( met.perp()*GeV < 150 )      vetoEvent;
      if ( !passTrig )                 vetoEvent;
      
      // Leading jet pt
      //double pTLeadJet = jets.at(0).pT();
        
      // Higgs candidate
      FourMomentum HiggsCand = bjets.at(0).momentum()+bjets.at(1).momentum();

      // Build A boson
      double mH = HiggsCand.mass();
      double mZ = 91.1876;
      
      // Build MET 4-vector setting pz to 0 and using mZ constraint
      FourMomentum met4;
      met4.setXYZE(met.x(), met.y(), 0, sqrt(pow(mZ,2)+pow(met.x(),2)+pow(met.y(),2)));
      
      // Remove longitudinal component from Higgs 
      FourMomentum HiggsCand_nlc;
      HiggsCand_nlc.setXYZE(HiggsCand.px(), HiggsCand.py(), 0, sqrt(pow(mH,2)+pow(HiggsCand.px(),2)+pow(HiggsCand.py(),2)));
      double pTA = (HiggsCand_nlc+met4).pT();
      double mTA = (HiggsCand_nlc+met4).mass();
      
      FourMomentum truth_met;
      if (isSignalSample) neutrinos.at(0).momentum()+neutrinos.at(1).momentum();
      
      // For debugging
      double dPhiMetH = deltaPhi(met, HiggsCand.p3());
      
      /*double dPhiMetb1 = deltaPhi(met, bjets.at(0).p3());
      double dPhiMetb2 = deltaPhi(met, bjets.at(1).p3());
      double dPhib1b2 = deltaPhi(bjets.at(0), bjets.at(1));
      double dPhiZH_truth = deltaPhi(Zboson, Higgs);
      double dRZH_truth = deltaR(Zboson, Higgs);
      double dPhiZMET = deltaPhi(Zboson, met);
      double dRZMET = deltaR(Zboson, met);
         
      std::cout << "Truth A boson pt = " << Aboson.pT()*GeV << " , reco pTA = " << pTA*GeV << std::endl;
      std::cout << "Truth dPhi(H,MET) = " << deltaPhi(Higgs.p3(), met) << " reco dPhi = " << dPhiMetH << " dPhi(met,b1) = "
      << dPhiMetb1 << " dPhi(met,b2) = " << dPhiMetb2 << " dPhi(b1,b2) = " << dPhib1b2 << std::endl;
      std::cout << "Truth dR(H,MET) = " << deltaR(Higgs.p3(), met) << " reco dR = " << deltaR(HiggsCand.p3(), met) << " dR(met,b1) = "
      << deltaR(met, bjets.at(0).p3()) << " dR(met,b2) = " << deltaR(met, bjets.at(1).p3()) << " dR(b1,b2) = " << deltaR(bjets.at(1).p3(), bjets.at(0).p3()) << std::endl;
      std::cout << "Truth dR(H,Z) = " << dRZH_truth << " dPhi(H,Z) = " << dPhiZH_truth << std::endl;
      std::cout << "dR(Z,met) = " << dRZMET << " dPhi(Z,met) = " << dPhiZMET << std::endl;
      std::cout << "Truth MET = " << truth_met.perp()*GeV << " reco MET = " << met.perp()*GeV << " dR(reco,truth MET) = " <<
      deltaR(met, truth_met) << std::endl << std::endl;*/
      
      // Reconstruct top mass proxy
      FourMomentum pb_closest, pb_furthest;
      if (deltaR(bjets.at(0).momentum(), met4) < deltaR(bjets.at(1).momentum(), met4)) {
        pb_closest  = bjets.at(0).momentum();
	pb_furthest = bjets.at(1).momentum();
      } else {
        pb_closest  = bjets.at(1).momentum();
	pb_furthest = bjets.at(0).momentum();
      }
      double mtop_closest  = std::sqrt(2.*pb_closest.pt()*met4.pt()*(1-cos(deltaPhi(pb_closest.ptvec(), met4.ptvec()))));
      double mtop_furthest = std::sqrt(2.*pb_furthest.pt()*met4.pt()*(1-cos(deltaPhi(pb_furthest.ptvec(), met4.ptvec()))));
      double mtop_average  = 0.5*(mtop_closest+mtop_furthest);
      
      // SR selection
      bool passesSR = false;
      if (dPhiMetH             > 1.0  &&
          njets                < 5    &&
	  mtop_average*GeV     > 175. &&
	  HiggsCand.mass()*GeV > 130. &&
	  bjets.at(0).pt()*GeV > 70.     ) passesSR = true;
      
      
      fprintf(fp, "%s , %f , %d , %f , %f , %f , %f , %f , %f , %f , %f , %f , %f , %f , %f , %f , %d\n", 
      m_sample.c_str(),
      event.weights()[0]*1000, njets,
      met4.x(), met4.y(), met4.z(), met4.E(), 
      bjets.at(0).momentum().x(), bjets.at(0).momentum().y(), bjets.at(0).momentum().z(),bjets.at(0).momentum().E(),
      bjets.at(1).momentum().x(), bjets.at(1).momentum().y(), bjets.at(1).momentum().z(),bjets.at(1).momentum().E(),
      mTA, passesSR);
      
      //std::cout << "mT = " << mTA*GeV << std::endl;

      // Fill histograms after selection
      _h["njets"]->fill(njets);
      _h["nbjets"]->fill(nbjets);
      _h["met"]->fill(met.perp()*GeV);
      if (passTrig) _h["met_afterTrig"]->fill(met.perp()*GeV);
      _h["nleptons"]->fill(electrons.size()+muons.size());
      _h["nfatjets"]->fill(nTrimmedFat);
      _h["ntrackjet"]->fill(associated_trackjets.size());
      _h["nbtagTJ"]->fill(nbjets_GATJ);
      _h["mH_firstBquarks"]->fill(mH_firstBquarks);
      _h["metSig"]->fill(met.perp()/std::sqrt(HT));
      if (isSignalSample) {
        _h["dR_firstBquarks"]->fill(deltaR(firstBQuarksFromHiggs.at(0).momentum(),firstBQuarksFromHiggs.at(1)));
        _h["mH_lastBquarks"]->fill(mH_lastBquarks); 
        _h["mH_firstBhadrons"]->fill(mH_firstBhadrons);
        _h["mH_lastBhadrons"]->fill(mH_lastBhadrons);
	_h["pt_Atruth"]->fill(Aboson.pT()*GeV);
	_h["dPhi_MetZ"]->fill(deltaPhi(Zboson, met));  
        _h["dR_MetZ"]->fill(deltaR(Zboson, met));
      }
      if (nTrimmedFat > 0) _h["mLeadFatJet"]->fill(trimmedFatJets.at(0).mass()*GeV); 
      _h["pmag_bfromH"]->fill(bfromH_maxp*GeV);
      _h["pmag_BfromH"]->fill(BfromH_maxp*GeV);
      _h["pt_HCand"]->fill(HiggsCand.pt()*GeV);
      _h["pt_b1"]->fill(bjets.at(0).pt()*GeV);	   
      _h["pt_b2"]->fill(bjets.at(1).pt()*GeV);
      if (ljets.size() > 0) _h["pt_j1"]->fill(ljets.at(0).pt()*GeV);
      _h["dR_b1b2"]->fill(deltaR(bjets.at(0).momentum(),bjets.at(1).momentum()));		
      _h["m_HCand"]->fill(HiggsCand.mass()*GeV);
      _h["pt_ACand"]->fill(pTA*GeV);
      _h["pt_ACand"]->fill(pTA*GeV);
      _h["mT_ACand"]->fill(mTA*GeV);	
      _h["dPhi_MetH"]->fill(dPhiMetH);  
      _h["dR_MetH"]->fill(deltaR(met, HiggsCand));   
      _h["mtop_closest"]->fill(mtop_closest*GeV);
      _h["mtop_furthest"]->fill(mtop_furthest*GeV);
      _h["mtop_average"]->fill(mtop_average*GeV);
      
      if(passesSR) {
      	_h["dPhi_MetH_SR"]->fill(dPhiMetH);
	_h["mtop_average_SR"]->fill(mtop_average*GeV);
	_h["njets_SR"]->fill(njets);
        _h["m_HCand_SR"]->fill(HiggsCand.mass()*GeV);
        _h["mT_ACand_SR"]->fill(mTA*GeV);
      }	     
    }


    void finalize() {
      fprintf(fp, "# Nevents_processed = %d , sumW_processed = %f\n" , m_nEvents_processed , m_sumWeights_processed);
      fclose(fp);
      /*
      // Normalize to cross-section
      const double sf = (crossSection() / sumOfWeights());
      for (auto hist : _h) {
        scale(hist.second, sf);
        // Normalized distributions
        if (hist.first.find("_norm") != string::npos)  normalize(hist.second);
      }
      */
    }
    
    void findNeutrinos(Particles &part, const Event & event) {
      const GenEvent *genEvent = event.genEvent();
      const vector<ConstGenParticlePtr> genParticles = HepMCUtils::particles(genEvent);
      for (auto p : genParticles) {
        if (part.size() == 2) break;
	if ( (p->pdg_id() == 12 || p->pdg_id() == 14 || p->pdg_id() == 16) && p->status() == 1) {
	  part.push_back(Particle(p));
	}
	else if ( (p->pdg_id() == -12 || p->pdg_id() == -14 || p->pdg_id() == -16) && p->status() == 1) {
	  part.push_back(Particle(p));
	}
      }
    }
    
    void findLastParticleById(Particle &part, const Event & event, const int id) {
      const GenEvent *genEvent = event.genEvent();
      const vector<ConstGenParticlePtr> genParticles = HepMCUtils::particles(genEvent);
      for (auto p : genParticles) {
        if ( p->pdg_id() == id) {
	  bool isLast = true;
          ConstGenVertexPtr dv = p->end_vertex();
          if (dv != nullptr) {
            // Loop over all daughter particles and check if they contain a particle with the same pdg id
            for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
              if (abs(pp->pdg_id()) == id) isLast = false;
            }
	  }
	  if (isLast) part = Particle(p);
	}
      }
    } 
    
    void findFirstParticlesFromDecay(const Particle &mother, Particles &daughters, const int daughterId) {
      ConstGenVertexPtr dv = mother.genParticle()->end_vertex();
      for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
        if (abs(pp->pdg_id()) == daughterId) daughters.push_back(Particle(pp));
      }
    }
    
    void findLastParticlesFromDecay(const Particle &mother, Particles &daughters, const int daughterId) {
      ConstGenVertexPtr dv = mother.genParticle()->end_vertex();
      for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
 	if (abs(pp->pdg_id()) == daughterId) {
	  ConstGenParticlePtr lastDaughter = pp;
	  while ( !isLastInChain(lastDaughter) ) {
	    ConstGenVertexPtr dv2 = lastDaughter->end_vertex();
	    for(ConstGenParticlePtr pp2: HepMCUtils::particles(dv2, Relatives::CHILDREN)){\
	      if ( pp2->pdg_id() == lastDaughter->pdg_id() ) {
	        lastDaughter = pp2;
	      }
	    }
	  }
	  daughters.push_back(lastDaughter);
	}	
      }
    }
    
    void findFirstBHadronsFromBquarks(const Particle &mother, Particles &daughters) {
      // Start with the mother
      ConstGenParticlePtr current = mother.genParticle();
      // Loop over daughters and check if any has a bottom flavour
      while (daughtersHaveB(current)) {
        // Get the decay vertex
        ConstGenVertexPtr dv = current->end_vertex();
        // Loop over daughters and find the one with the b-flavour
	for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
	  if ( PID::hasBottom(pp->pdg_id()) && PID::isHadron(pp->pdg_id()) ) { 
	    current = pp;
	    daughters.push_back(Particle(current));
	    return;
	  }
        }	
      }     
    }
    
    void findLastBHadronsFromDecay(const Particle &mother, Particles &daughters) {
      // Start with the mother
      ConstGenParticlePtr current = mother.genParticle();
      // Loop over daughters and check if any has a bottom flavour
      while (daughtersHaveB(current)) {
        // Get the decay vertex
        ConstGenVertexPtr dv = current->end_vertex();
        // Loop over daughters and find the one with the b-flavour
	for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
          if ( PID::hasBottom(pp->pdg_id()) ) { 
	    current = pp;
	    break;
	  }
        }	
      }
      daughters.push_back(Particle(current));
    }
    
    bool isLastInChain(ConstGenParticlePtr p) {
      bool isLast = true;
      ConstGenVertexPtr dv = p->end_vertex();
      for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
        if ( pp->pdg_id() == p->pdg_id() ) { isLast = false; break; }
      }
      return isLast;
    }
    
    bool daughtersHaveB(ConstGenParticlePtr p) {
      bool containBhad = false;
      ConstGenVertexPtr dv = p->end_vertex();
      for(ConstGenParticlePtr pp: HepMCUtils::particles(dv, Relatives::CHILDREN)){
        //std::cout << "Daughter of " << p->pdg_id() << " = " << pp->pdg_id() << std::endl;
        if ( PID::hasBottom(pp->pdg_id()) ) { containBhad = true; break; }
      }
      return containBhad;
    }
    
  private:

    /// @name Objects that are used by the event selection decisions
    map<string, Histo1DPtr> _h;
    
    bool m_smear = false;
    double m_sumWeights_processed = 0;
    int m_nEvents_processed = 0;
    std::string m_sample = "";
    FILE *fp;

  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(AZH_vvbb);


}
