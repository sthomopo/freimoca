// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/PromptFinalState.hh"

#define PI 3.1415926

namespace Rivet {


  /// @brief Add a short analysis description here
  class HWWAnalysis : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(HWWAnalysis);


    /// @name Analysis methods
    //@{
 
    /// Book histograms and initialise projections before the run
    void init() {

      // Initialise and register projections

      // The basic final-state projection:
      // all final-state particles within
      // the given eta acceptance
      const FinalState fs(Cuts::abseta < 4.9);
      // does this select only particles or also jets?

      // The final-state particles declared above are clustered using FastJet with
      // the anti-kT algorithm and a jet-radius parameter 0.4
      // muons and neutrinos are excluded from the clustering
      FastJets jetfs(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      declare(jetfs, "jets");

      // FinalState of prompt photons and bare muons and electrons in the event
      PromptFinalState photons(Cuts::abspid == PID::PHOTON);
      PromptFinalState bare_leps(Cuts::abspid == PID::MUON || Cuts::abspid == PID::ELECTRON);

      // Dress the prompt bare leptons with prompt photons within dR < 0.1,
      // and apply some fiducial cuts on the dressed leptons
      Cut lepton_cuts = Cuts::abseta < 2.5 && Cuts::pT > 10*GeV;
      DressedLeptons dressed_leps(photons, bare_leps, 0.1, lepton_cuts);
      declare(dressed_leps, "leptons");

      // Missing momentum
      declare(MissingMomentum(fs), "MET");

      // Book histograms
      
      book(_h["pres_MET"], "pres_MET", 50, 0.0, 500.0);
      

      // Jets

      book(_h["pres_N_jets"], "pres_N_jets", 7, -0.5, 6.5);

      book(_h["none_pT_jet1"], "none_pT_jet1", 40, 0.0, 400.0);
      book(_h["pres_pT_jet1"], "pres_pT_jet1", 40, 0.0, 400.0);
      book(_h["wwsr_pT_jet1"], "wwsr_pT_jet1", 40, 0.0, 400.0);
      book(_h["wwcr_pT_jet1"], "wwcr_pT_jet1", 40, 0.0, 400.0);

      book(_h["none_pT_jet2"], "none_pT_jet2", 40, 0.0, 400.0);
      book(_h["pres_pT_jet2"], "pres_pT_jet2", 40, 0.0, 400.0);
      book(_h["wwsr_pT_jet2"], "wwsr_pT_jet2", 40, 0.0, 400.0);

      book(_h["none_pT_jet3"], "none_pT_jet3", 40, 0.0, 400.0);
      book(_h["pres_pT_jet3"], "pres_pT_jet3", 40, 0.0, 400.0);
      book(_h["wwsr_pT_jet3"], "wwsr_pT_jet3", 40, 0.0, 400.0);
 
      book(_h["pres_eta_jet1"], "pres_eta_jet1", 50, 0.0, 4.5);

      book(_h["pres_eta_jet2"], "pres_eta_jet2", 50, 0.0, 4.5);

      book(_h["pres_eta_jet3"], "pres_eta_jet3", 50, 0.0, 4.5);


      book(_h["pres_HT"], "pres_HT", 20, 0.0, 200.0);

      // mll

      book(_h["none_mll_0jet"], "none_mll_0jet", 50, 0.0, 250.0);
      book(_h["pres_mll_0jet"], "pres_mll_0jet", 50, 0.0, 250.0);
      book(_h["wwsr_mll_0jet"], "wwsr_mll_0jet", 11, 0.0, 55.0);
      book(_h["wwcr_mll_0jet"], "wwcr_mll_0jet", 50, 0.0, 250.0);

      book(_h["none_mll_1jet"], "none_mll_1jet", 50, 0.0, 250.0);
      book(_h["pres_mll_1jet"], "pres_mll_1jet", 50, 0.0, 250.0);
      book(_h["wwsr_mll_1jet"], "wwsr_mll_1jet", 11, 0.0, 55.0);
      book(_h["wwcr_mll_1jet"], "wwcr_mll_1jet", 50, 0.0, 250.0);

      //book(_h[""], "", , , );
      
      ///////////////////
      // Cut Variables //
      ///////////////////

      // Background rejection
     
      // Cut Variables
      book(_h["bgre_N_bjet"], "bgre_N_bjet", 7, -0.5, 6.5);
      
      book(_h["bgre_dPhillMET"], "bgre_dPhillMET", 50, 0.0, 22/7);
      book(_h["bgre_pTll"], "bgre_pTll", 50, 0.0, 200.0);
      
      book(_h["bgre_mTl1"], "bgre_mTl1", 50, 0.0, 200.0);
      book(_h["bgre_mTl2"], "bgre_mTl2", 50, 0.0, 200.0);
      book(_h["bgre_mtautau"], "bgre_mtautau", 50, 0.0, 200.0);
      book(_h["wwcr_mtautau"], "wwcr_mtautau", 50, 0.0, 200.0);

      // HWW topology

      book(_h["pres_dPhill_0jet"], "pres_dPhill_0jet", 220/7, 0.0, 22/7);
      book(_h["pres_dPhill_1jet"], "pres_dPhill_1jet", 220/7, 0.0, 22/7);
      book(_h["wwsr_dPhill_0jet"], "wwsr_dPhill_0jet", 18, 0.0, 1.8);
      book(_h["wwsr_dPhill_1jet"], "wwsr_dPhill_1jet", 18, 0.0, 1.8);
      
      ///////////////////

      // Leptons

      book(_h["none_pT_leadLep"], "none_pT_leadLep", 51, 0.0, 102.0);
      book(_h["none_pT_sublLep"], "none_pT_sublLep", 34, 0.0, 102.0);
      book(_h["pres_pT_leadLep"], "pres_pT_leadLep", 51, 0.0, 102.0);
      book(_h["pres_pT_sublLep"], "pres_pT_sublLep", 34, 0.0, 102.0);
      book(_h["wwsr_pT_leadLep"], "wwsr_pT_leadLep", 51, 0.0, 102.0);
      book(_h["wwsr_pT_sublLep"], "wwsr_pT_sublLep", 34, 0.0, 102.0);
      book(_h["wwcr_pT_leadLep_0jet"], "wwcr_pT_leadLep_0jet", 51, 0.0, 102.0);
      book(_h["wwcr_pT_leadLep_1jet"], "wwcr_pT_leadLep_1jet", 51, 0.0, 102.0);
      book(_h["wwcr_pT_sublLep_0jet"], "wwcr_pT_sublLep_0jet", 34, 0.0, 102.0);
      book(_h["wwcr_pT_sublLep_1jet"], "wwcr_pT_sublLep_1jet", 34, 0.0, 102.0);

      book(_h["pres_eta_leadLep"], "pres_eta_leadLep", 50, 0.0, 5.0);
      book(_h["pres_eta_sublLep"], "pres_eta_sublLep", 50, 0.0, 5.0);


      //book(_h["mT"], "mT", 50, 0.0, 250.0);
      
      // Higgs

      book(_h["none_pT_H"], "none_pT_H", 50, 0.0, 100.0);
      book(_h["pres_pT_H"], "pres_pT_H", 50, 0.0, 100.0);
      book(_h["wwsr_pT_H"], "wwsr_pT_H", 50, 0.0, 100.0);
      book(_h["wwcr_pT_H_0jet"], "wwcr_pT_H_0jet", 50, 0.0, 100.0);
      book(_h["wwcr_pT_H_1jet"], "wwcr_pT_H_1jet", 50, 0.0, 100.0);

      // mT

      book(_h["none_mT_0jet"], "none_mT_0jet", 30, 0.0, 300.0);
      book(_h["pres_mT_0jet"], "pres_mT_0jet", 30, 0.0, 300.0);
      book(_h["wwsr_mT_0jet"], "wwsr_mT_0jet", 20, 50.0, 250.0);
      book(_h["wwcr_mT_0jet"], "wwcr_mT_0jet", 25, 0.0, 250.0);

      book(_h["none_mT_1jet"], "none_mT_1jet", 30, 0.0, 300.0);
      book(_h["pres_mT_1jet"], "pres_mT_1jet", 30, 0.0, 300.0);
      book(_h["wwsr_mT_1jet"], "wwsr_mT_1jet", 20, 50.0, 250.0);
      book(_h["wwcr_mT_1jet"], "wwcr_mT_1jet", 20, 50.0, 250.0);

      book(_h["mT_0jet_0mll_0pTsubl_sublE"], "mT_0jet_0mll_0pTsubl_sublE", 20, 50.0, 250.0);
      book(_h["mT_0jet_0mll_0pTsubl_sublM"], "mT_0jet_0mll_0pTsubl_sublM", 20, 50.0, 250.0);
      book(_h["mT_0jet_0mll_1pTsubl_sublE"], "mT_0jet_0mll_1pTsubl_sublE", 20, 50.0, 250.0);
      book(_h["mT_0jet_1mll_0pTsubl_sublE"], "mT_0jet_1mll_0pTsubl_sublE", 20, 50.0, 250.0);

      book(_h["mT_0jet_0mll_1pTsubl_sublM"], "mT_0jet_0mll_1pTsubl_sublM", 20, 50.0, 250.0);
      book(_h["mT_0jet_1mll_0pTsubl_sublM"], "mT_0jet_1mll_0pTsubl_sublM", 20, 50.0, 250.0);
      book(_h["mT_0jet_1mll_1pTsubl_sublE"], "mT_0jet_1mll_1pTsubl_sublE", 20, 50.0, 250.0);
      book(_h["mT_0jet_1mll_1pTsubl_sublM"], "mT_0jet_1mll_1pTsubl_sublM", 20, 50.0, 250.0);

      book(_h["mT_1jet_0mll_0pTsubl_sublE"], "mT_1jet_0mll_0pTsubl_sublE", 20, 50.0, 250.0);
      book(_h["mT_1jet_0mll_0pTsubl_sublM"], "mT_1jet_0mll_0pTsubl_sublM", 20, 50.0, 250.0);
      book(_h["mT_1jet_0mll_1pTsubl_sublE"], "mT_1jet_0mll_1pTsubl_sublE", 20, 50.0, 250.0);
      book(_h["mT_1jet_1mll_0pTsubl_sublE"], "mT_1jet_1mll_0pTsubl_sublE", 20, 50.0, 250.0);

      book(_h["mT_1jet_0mll_1pTsubl_sublM"], "mT_1jet_0mll_1pTsubl_sublM", 20, 50.0, 250.0);
      book(_h["mT_1jet_1mll_0pTsubl_sublM"], "mT_1jet_1mll_0pTsubl_sublM", 20, 50.0, 250.0);
      book(_h["mT_1jet_1mll_1pTsubl_sublE"], "mT_1jet_1mll_1pTsubl_sublE", 20, 50.0, 250.0);
      book(_h["mT_1jet_1mll_1pTsubl_sublM"], "mT_1jet_1mll_1pTsubl_sublM", 20, 50.0, 250.0);



      ////////
      // CR //
      ////////
      
      //beide: Nbjet
      //1jet: mll, phill
      //2jet: mll, mtautau, mTl

      // Other ways to specify histogram binning s
      //book(_h["YYYY"], "myh2", logspace(20, 1e-2, 1e3));
      //book(_h["ZZZZ"], "myh3", {0.0, 1.0, 2.0, 4.0, 8.0, 16.0});
      // take binning from reference data using HEPData ID (digits in "d01-x01-y01" etc.)
      // book(_h["AAAA"], 1, 1, 1);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      // Retrieve dressed leptons, sorted by pT
      vector<DressedLepton> leptons = apply<DressedLeptons>(event, "leptons").dressedLeptons();

      // Retrieve clustered jets, sorted by pT, with a minimum pT cut
      Jets jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 30*GeV && Cuts::abseta < 4.5);

      // Remove all jets within dR < 0.2 of a dressed lepton
      idiscardIfAnyDeltaRLess(jets, leptons, 0.2);

      // Select jets ghost-associated to B-hadrons with a certain fiducial selection
      // Jets bjets = filter_select(jets, [](const Jet& jet) {
      //  return  jet.bTagged(Cuts::pT > 5*GeV && Cuts::abseta < 2.5);
      //});
      
      Jets bjets = filter_select(jets, [](const Jet& jet) {
        return  jet.bTagged(Cuts::pT > 20*GeV);
      });
      
      //cout << "Message: Variables" << endl;
 
      // Retrieve quantities that we need for the analysis selection from projections
      double MET = apply<MissingMomentum>(event, "MET").missingPt();
      Vector3 VMET = apply<MissingMomentum>(event, "MET").vectorMissingPt();
      
      
      // delete later TODO
      double ptmiss = MET;
      
      
      
      if (leptons.size() != 2) vetoEvent;

      // dilepton mass
      FourMomentum dilept = leptons.at(0).momentum() + leptons.at(1).momentum();
      double mll = dilept.mass();
      
      //double dPhill = abs(leptons.at(0).azimuthalAngle() - leptons.at(1).azimuthalAngle());
      double dPhill = deltaPhi(leptons.at(0), leptons.at(1));


      // Discriminant Variable mT
      
      double ETll = sqrt( dilept.pTvec().polarRadius2() + dilept.mass2() );
      double mT_E = (ETll + MET);
      Vector3 mT_p = (dilept.pTvec() + VMET);
      
      double mT = sqrt( pow( mT_E, 2) - mT_p.polarRadius2() );
      
      //cout << "new:" << endl;
      //cout << ETll << endl;
      //cout << mT_E << endl;
      //cout << mT_p << endl;
      //cout << mT << endl;

      //double deltaphil1 = abs(leptons.at(0).azimuthalAngle() - VMET.azimuthalAngle());
      double deltaphil1 = deltaPhi(leptons.at(0), VMET);
      double mTl1 = sqrt( 2 * leptons.at(0).momentum().pT() * MET * (1 -  cos( deltaphil1 )) );
      //double deltaphil2 = abs(leptons.at(1).azimuthalAngle() - VMET.azimuthalAngle());
      double deltaphil2 = deltaPhi(leptons.at(1), VMET);
      double mTl2 = sqrt( 2 * leptons.at(1).momentum().pT() * MET * (1 -  cos( deltaphil2 )) );

      // 1jet pt and eta

      bool jet1 = (jets.size() >= 1);
      bool jet2 = (jets.size() >= 2);
      bool jet3 = (jets.size() >= 3);

      double pT_jet1=-1;
      double eta_jet1=-1;
      double pT_jet2=-1;
      double eta_jet2=-1;
      double pT_jet3=-1;
      double eta_jet3=-1;

      if (jets.size() > 0) {
         pT_jet1=jets.at(0).momentum().pT();
         eta_jet1=jets.at(0).momentum().abseta();
         if (jets.size() > 1) {
            pT_jet2=jets.at(1).momentum().pT();
            eta_jet2=jets.at(1).momentum().abseta();
            if (jets.size() > 2) {
               pT_jet3=jets.at(2).momentum().pT();
               eta_jet3=jets.at(2).momentum().abseta();
            }
         }
      }

      // HT = summe aller pT_jets
     
      double HT = 0;
      //cout << "new HT" << endl;
      for (int i = 0; i < abs(jets.size()); i++) {
         HT = HT + jets.at(i).pT();
         //cout << HT << endl;
      }

      Vector3 pT_H_vec = leptons.at(0).pTvec() + leptons.at(1).pTvec() + VMET; 

      double pT_H = pT_H_vec.polarRadius();

      const double lep0Px = leptons.at(0).px();
      const double lep0Py = leptons.at(0).py();
      const double lep1Px = leptons.at(1).px();
      const double lep1Py = leptons.at(1).py();
      const double mpx = VMET.x();
      const double mpy = VMET.y();
      const double x_1 = (lep0Px*lep1Py-lep0Py*lep1Px) / (lep1Py*mpx-lep1Px*mpy+lep0Px*lep1Py-lep0Py*lep1Px);
      const double x_2 = (lep0Px*lep1Py-lep0Py*lep1Px) / (lep0Px*mpy-lep0Py*mpx+lep0Px*lep1Py-lep0Py*lep1Px);
      double mtautau = -9.0;
      if(x_1 > 0.0 && x_2 > 0.0){ mtautau = mll/std::sqrt(x_1*x_2); }



	     
      /////////////////////////////
      // Fill Histograms No Cuts //
      /////////////////////////////
      
      //cout << "Message: empty Hists" << endl;

      if (jet1) _h["none_pT_jet1"]->fill(pT_jet1/GeV);
      if (jet2) _h["none_pT_jet2"]->fill(pT_jet2/GeV);
      if (jet3) _h["none_pT_jet3"]->fill(pT_jet3/GeV);

      _h["none_pT_leadLep"]->fill(leptons.at(0).momentum().pT()/GeV);
      _h["none_pT_sublLep"]->fill(leptons.at(1).momentum().pT()/GeV);

      //_h["none_pT_H"]->fill(var/unit);

      if (jets.size() == 0) {
         _h["none_mT_0jet"]->fill(mT/GeV);
         _h["none_mll_0jet"]->fill(mll/GeV);
      }
      if (jets.size() == 1) {
         _h["none_mT_1jet"]->fill(mT/GeV);
         _h["none_mll_1jet"]->fill(mll/GeV);
      }

      /////////////////////////////

      // Analysis cuts

      ///////////////////////
      // Preselection Cuts //
      ///////////////////////

      //cout << "Message: pres Cuts" << endl;

      // Apply a missing-momentum cut
      //if (MET < 30*GeV) vetoEvent;
      
      
      // discard if leptons have same charge
      if ( leptons.at(0).charge() * leptons.at(1).charge() > 0 ) vetoEvent;

      // discard if leptons are not electron and muon
      bool leptonsEM = (leptons.at(0).abspid() == PID::ELECTRON && leptons.at(1).abspid() == PID::MUON);
      bool leptonsME = (leptons.at(0).abspid() == PID::MUON && leptons.at(1).abspid() == PID::ELECTRON);


      if ( not leptonsEM && not leptonsME ) vetoEvent;

      // discard pTlead < 22 GeV and pT sublead < 15 GeV
      if (leptons.at(0).momentum().pT() < 22*GeV) vetoEvent;
      if (leptons.at(1).momentum().pT() < 15*GeV) vetoEvent;
      
      // dilepton mass Cut
      if (mll < 10*GeV) vetoEvent;      
     
      // ptmiss Cut
      if (ptmiss < 20*GeV) vetoEvent;

      ///////////////////////////////
      // Fill Histograms Preselect //
      ///////////////////////////////

      _h["pres_MET"]->fill(MET/GeV);

      _h["pres_N_jets"]->fill(jets.size());

      if (jet1) {
         _h["pres_pT_jet1"]->fill(pT_jet1/GeV);
         _h["pres_eta_jet1"]->fill(eta_jet1/GeV);
      }
      if (jet2) {
         _h["pres_pT_jet2"]->fill(pT_jet2/GeV);
         _h["pres_eta_jet2"]->fill(eta_jet2/GeV);
      }
      if (jet3) {
         _h["pres_pT_jet3"]->fill(pT_jet3/GeV);
         _h["pres_eta_jet3"]->fill(eta_jet3/GeV);
      }



      _h["pres_eta_leadLep"]->fill(leptons.at(0).momentum().abseta()/GeV);
      _h["pres_eta_sublLep"]->fill(leptons.at(1).momentum().abseta()/GeV);

      _h["pres_HT"]->fill(HT/GeV);



      _h["pres_pT_leadLep"]->fill(leptons.at(0).momentum().pT()/GeV);
      _h["pres_pT_sublLep"]->fill(leptons.at(1).momentum().pT()/GeV);

      _h["pres_pT_H"]->fill(pT_H/GeV);

      if (jets.size() == 0) {
         _h["pres_mT_0jet"]->fill(mT/GeV);
         _h["pres_mll_0jet"]->fill(mll/GeV);
         _h["pres_dPhill_0jet"]->fill(dPhill);
      }
      if (jets.size() == 1) {
         _h["pres_mT_1jet"]->fill(mT/GeV);
         _h["pres_mll_1jet"]->fill(mll/GeV);
         _h["pres_dPhill_1jet"]->fill(dPhill);
      }

      ///////////////////////////////

      
      //////////
      // WWCR //
      //////////

      //////////////////////////
      // Fill histograms WWCR //
      //////////////////////////
      
      //cout << "Message: WWCR Cuts" << endl;

      bool CR1 = (bjets.size() == 0);
      
      if (jets.size() == 0) {
         bool CR1mll = (mll > 55*GeV || mll < 110*GeV);
         bool CR1dPhill = (dPhill < 2.6);
	 if (CR1 && CR1mll && CR1dPhill) {
	    _h["wwcr_pT_leadLep_0jet"]->fill(leptons.at(0).momentum().pT()/GeV);
	    _h["wwcr_pT_sublLep_0jet"]->fill(leptons.at(1).momentum().pT()/GeV);

	    _h["wwcr_pT_H_0jet"]->fill(pT_H/GeV);

	    _h["wwcr_mT_0jet"]->fill(mT/GeV);
	    _h["wwcr_mll_0jet"]->fill(mll/GeV);
	 }
      }
      
      if (jets.size() == 1) {
         bool CR2mll = (mll < 80*GeV);
         //cout << "tautau cr" << endl;
         bool CR2mtautau = ( abs(91.1876*GeV - mtautau) < 25*GeV);
         bool CR2maxmTl = ( mTl1 >= mTl2 && mTl1 < 50*GeV )  ||  ( mTl2 > mTl1 && mTl2 < 50*GeV );
	 if (CR1 && CR2mll && CR2mtautau && CR2maxmTl) {
	    _h["wwcr_pT_jet1"]->fill(pT_jet1/GeV);


            _h["wwcr_mtautau"]->fill(mll/GeV);
	    
	    _h["wwcr_pT_leadLep_1jet"]->fill(leptons.at(0).momentum().pT()/GeV);
	    _h["wwcr_pT_sublLep_1jet"]->fill(leptons.at(1).momentum().pT()/GeV);

	    _h["wwcr_pT_H_1jet"]->fill(pT_H/GeV);

	    _h["wwcr_mT_1jet"]->fill(mT/GeV);
	    _h["wwcr_mll_1jet"]->fill(mll/GeV);
	 }
         
      }
 


      //////////////////////////
      // Background rejection //
      //////////////////////////
      
      //cout << "Message: BGRE Cuts" << endl;

      // backGroudnREjection bgre Histograms:
      
      // rejecting b-jets with pT > 20 GeV
      if (bjets.size() > 0) vetoEvent;
      
      _h["bgre_N_bjet"]->fill(bjets.size());
      
      
      if (jets.size() == 0 ) {
         //double dPhillMET = abs(dilept.azimuthalAngle() - VMET.azimuthalAngle());
         double dPhillMET = deltaPhi(dilept, VMET);
	 
	 //delta phi ll ETmiss > pi/2
	 if (dPhillMET < 11/7.0 || dPhillMET > 33/7.0) vetoEvent;
	 
	 //pTll > 30 GeV
	 if (dilept.pT() < 30*GeV) vetoEvent;
	 
         _h["bgre_dPhillMET"]->fill(dPhillMET);
	 _h["bgre_pTll"]->fill(dilept.pT());
      }
     
      if (jets.size() == 1 ) {
 
	 // max(mTl) > 50 GeV Cut
	 if (mTl1 >= mTl2) {
	    if (mTl1 < 50*GeV) vetoEvent;
	 } else {
	    if (mTl2 < 50*GeV) vetoEvent;
	 }
	 
	 //cout << "tautau" << endl;
	 if (91.1876*GeV < (mtautau - 25*GeV) ) vetoEvent;
         _h["bgre_mtautau"]->fill(mll/GeV);
	 
         _h["bgre_mTl1"]->fill(mTl1/GeV);
         _h["bgre_mTl2"]->fill(mTl2/GeV);
      }


      ////////////////////////////////
      // H -> WW* -> evmuv Topology //
      ////////////////////////////////
     
      //cout << "Message: Topology Cuts" << endl;
 
      if (mll > 55*GeV) vetoEvent;
      
      if (dPhill > 1.8 && dPhill < 44/7.0 - 1.8) vetoEvent;
           
      
      //////////////////////////
      // Fill histograms WWSR //
      //////////////////////////

      
      if (jet1) _h["wwsr_pT_jet1"]->fill(pT_jet1/GeV);
      if (jet2) _h["wwsr_pT_jet2"]->fill(pT_jet2/GeV);
      if (jet3) _h["wwsr_pT_jet3"]->fill(pT_jet3/GeV);

      _h["wwsr_pT_leadLep"]->fill(leptons.at(0).momentum().pT()/GeV);
      _h["wwsr_pT_sublLep"]->fill(leptons.at(1).momentum().pT()/GeV);

      _h["wwsr_pT_H"]->fill(pT_H/GeV);

      if (jets.size() == 0) {
         _h["wwsr_mT_0jet"]->fill(mT/GeV);
         _h["wwsr_mll_0jet"]->fill(mll/GeV);
         _h["wwsr_dPhill_0jet"]->fill(dPhill);
      }
      if (jets.size() == 1) {
         _h["wwsr_mT_1jet"]->fill(mT/GeV);
         _h["wwsr_mll_1jet"]->fill(mll/GeV);
         _h["wwsr_dPhill_1jet"]->fill(dPhill);
      }

      bool SRmll = (mll >= 30*GeV);
      bool SRpTsub = (leptons.at(1).momentum().pT() >= 20*GeV);
      bool SRflsub = (leptons.at(1).abspid() == PID::MUON);



      if (jets.size() == 0) {
         if(!SRmll && !SRpTsub && !SRflsub) _h["mT_0jet_0mll_0pTsubl_sublE"]->fill(mT/GeV);
         if(!SRmll && !SRpTsub &&  SRflsub) _h["mT_0jet_0mll_0pTsubl_sublM"]->fill(mT/GeV);
         if(!SRmll &&  SRpTsub && !SRflsub) _h["mT_0jet_0mll_1pTsubl_sublE"]->fill(mT/GeV);
         if( SRmll && !SRpTsub && !SRflsub) _h["mT_0jet_1mll_0pTsubl_sublE"]->fill(mT/GeV);

         if(!SRmll &&  SRpTsub &&  SRflsub) _h["mT_0jet_0mll_1pTsubl_sublM"]->fill(mT/GeV);
         if( SRmll && !SRpTsub &&  SRflsub) _h["mT_0jet_1mll_0pTsubl_sublM"]->fill(mT/GeV);
         if( SRmll &&  SRpTsub && !SRflsub) _h["mT_0jet_1mll_1pTsubl_sublE"]->fill(mT/GeV);
         if( SRmll &&  SRpTsub &&  SRflsub) _h["mT_0jet_1mll_1pTsubl_sublM"]->fill(mT/GeV);
      }

      if (jets.size() == 1) {
         if(!SRmll && !SRpTsub && !SRflsub) _h["mT_1jet_0mll_0pTsubl_sublE"]->fill(mT/GeV);
         if(!SRmll && !SRpTsub &&  SRflsub) _h["mT_1jet_0mll_0pTsubl_sublM"]->fill(mT/GeV);
         if(!SRmll &&  SRpTsub && !SRflsub) _h["mT_1jet_0mll_1pTsubl_sublE"]->fill(mT/GeV);
         if( SRmll && !SRpTsub && !SRflsub) _h["mT_1jet_1mll_0pTsubl_sublE"]->fill(mT/GeV);

         if(!SRmll &&  SRpTsub &&  SRflsub) _h["mT_1jet_0mll_1pTsubl_sublM"]->fill(mT/GeV);
         if( SRmll && !SRpTsub &&  SRflsub) _h["mT_1jet_1mll_0pTsubl_sublM"]->fill(mT/GeV);
         if( SRmll &&  SRpTsub && !SRflsub) _h["mT_1jet_1mll_1pTsubl_sublE"]->fill(mT/GeV);
         if( SRmll &&  SRpTsub &&  SRflsub) _h["mT_1jet_1mll_1pTsubl_sublM"]->fill(mT/GeV);
      }
      ////////////////////////// 

      // fill histogramms for Njet = 0 and Njet = 1:
      //if (jets.size() == 0) {
      // _h["MT0"]->fill(MT0/GeV);
      //}
      
      //if (jets.size() == 1) {
      // _h["MT0"]->fill(MT1/GeV);
      //}


    }


    /// Normalise histograms etc., after the run
    void finalize() {
      
      scale(_h["pres_MET"], crossSection()/picobarn/sumW());
      scale(_h["pres_N_jets"], crossSection()/picobarn/sumW());
      scale(_h["none_pT_jet1"], crossSection()/picobarn/sumW());
      scale(_h["pres_pT_jet1"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_pT_jet1"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_pT_jet1"], crossSection()/picobarn/sumW());
      scale(_h["none_pT_jet2"], crossSection()/picobarn/sumW());
      scale(_h["pres_pT_jet2"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_pT_jet2"], crossSection()/picobarn/sumW());
      scale(_h["none_pT_jet3"], crossSection()/picobarn/sumW());
      scale(_h["pres_pT_jet3"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_pT_jet3"], crossSection()/picobarn/sumW());
      scale(_h["pres_eta_jet1"], crossSection()/picobarn/sumW());
      scale(_h["pres_eta_jet2"], crossSection()/picobarn/sumW());
      scale(_h["pres_eta_jet3"], crossSection()/picobarn/sumW());
      scale(_h["pres_HT"], crossSection()/picobarn/sumW());
      scale(_h["none_mll_0jet"], crossSection()/picobarn/sumW());
      scale(_h["pres_mll_0jet"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_mll_0jet"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_mll_0jet"], crossSection()/picobarn/sumW());
      scale(_h["none_mll_1jet"], crossSection()/picobarn/sumW());
      scale(_h["pres_mll_1jet"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_mll_1jet"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_mll_1jet"], crossSection()/picobarn/sumW());
      scale(_h["bgre_N_bjet"], crossSection()/picobarn/sumW());
      scale(_h["bgre_dPhillMET"], crossSection()/picobarn/sumW());
      scale(_h["bgre_pTll"], crossSection()/picobarn/sumW());
      scale(_h["bgre_mTl1"], crossSection()/picobarn/sumW());
      scale(_h["bgre_mTl2"], crossSection()/picobarn/sumW());
      scale(_h["bgre_mtautau"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_mtautau"], crossSection()/picobarn/sumW());
      scale(_h["pres_dPhill_0jet"], crossSection()/picobarn/sumW());
      scale(_h["pres_dPhill_1jet"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_dPhill_0jet"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_dPhill_1jet"], crossSection()/picobarn/sumW());
      scale(_h["none_pT_leadLep"], crossSection()/picobarn/sumW());
      scale(_h["none_pT_sublLep"], crossSection()/picobarn/sumW());
      scale(_h["pres_pT_leadLep"], crossSection()/picobarn/sumW());
      scale(_h["pres_pT_sublLep"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_pT_leadLep"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_pT_sublLep"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_pT_leadLep_0jet"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_pT_leadLep_1jet"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_pT_sublLep_0jet"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_pT_sublLep_1jet"], crossSection()/picobarn/sumW());
      scale(_h["pres_eta_leadLep"], crossSection()/picobarn/sumW());
      scale(_h["pres_eta_sublLep"], crossSection()/picobarn/sumW());
      scale(_h["none_pT_H"], crossSection()/picobarn/sumW());
      scale(_h["pres_pT_H"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_pT_H"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_pT_H"], crossSection()/picobarn/sumW());
      scale(_h["none_mT_0jet"], crossSection()/picobarn/sumW());
      scale(_h["pres_mT_0jet"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_mT_0jet"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_mT_0jet"], crossSection()/picobarn/sumW());
      scale(_h["none_mT_1jet"], crossSection()/picobarn/sumW());
      scale(_h["pres_mT_1jet"], crossSection()/picobarn/sumW());
      scale(_h["wwsr_mT_1jet"], crossSection()/picobarn/sumW());
      scale(_h["wwcr_mT_1jet"], crossSection()/picobarn/sumW());
      scale(_h["mT_0jet_0mll_0pTsubl_sublE"], crossSection()/picobarn/sumW());
      scale(_h["mT_0jet_0mll_0pTsubl_sublM"], crossSection()/picobarn/sumW());
      scale(_h["mT_0jet_0mll_1pTsubl_sublE"], crossSection()/picobarn/sumW());
      scale(_h["mT_0jet_1mll_0pTsubl_sublE"], crossSection()/picobarn/sumW());
      scale(_h["mT_0jet_0mll_1pTsubl_sublM"], crossSection()/picobarn/sumW());
      scale(_h["mT_0jet_1mll_0pTsubl_sublM"], crossSection()/picobarn/sumW());
      scale(_h["mT_0jet_1mll_1pTsubl_sublE"], crossSection()/picobarn/sumW());
      scale(_h["mT_0jet_1mll_1pTsubl_sublM"], crossSection()/picobarn/sumW());
      scale(_h["mT_1jet_0mll_0pTsubl_sublE"], crossSection()/picobarn/sumW());
      scale(_h["mT_1jet_0mll_0pTsubl_sublM"], crossSection()/picobarn/sumW());
      scale(_h["mT_1jet_0mll_1pTsubl_sublE"], crossSection()/picobarn/sumW());
      scale(_h["mT_1jet_1mll_0pTsubl_sublE"], crossSection()/picobarn/sumW());
      scale(_h["mT_1jet_0mll_1pTsubl_sublM"], crossSection()/picobarn/sumW());
      scale(_h["mT_1jet_1mll_0pTsubl_sublM"], crossSection()/picobarn/sumW());
      scale(_h["mT_1jet_1mll_1pTsubl_sublE"], crossSection()/picobarn/sumW());
      scale(_h["mT_1jet_1mll_1pTsubl_sublM"], crossSection()/picobarn/sumW());

      //scale(_h["MET"], crossSection()/picobarn/sumW());
     
      //scale(_h["pT_lead_Lepton"], crossSection()/picobarn/sumW());
      
      //scale(_h["pT_subl_Lepton"], crossSection()/picobarn/sumW());
       
      //normalize(_h["XXXX"]); // normalize to unity
      //normalize(_h["YYYY"], crossSection()/picobarn); // normalize to generated cross-section in fb (no cuts)
      //scale(_h["ZZZZ"], crossSection()/picobarn/sumW()); // norm to generated cross-section in pb (after cuts)

    }

    //@}


    /// @name Histograms
    //@{
    map<string, Histo1DPtr> _h;
    //map<string, Profile1DPtr> _p;
    //map<string, CounterPtr> _c;
    //@}


  };


  DECLARE_RIVET_PLUGIN(HWWAnalysis);

}
