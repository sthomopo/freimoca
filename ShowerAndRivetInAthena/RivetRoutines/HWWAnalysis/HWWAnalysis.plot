BEGIN PLOT /HWWAnalysis/MET
Title="E_{T}^{miss}"
XLabel="$E_{T}^{\mathrm{miss}}$ [GeV]"
YLabel="dN/d$E_{T}^{\mathrm{miss}}$ [GeV$^{-1}]$"
END PLOT
