// Program for reading in LHEF and showering 
// Output into hepmc file

#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#include "HepMC/GenEvent.h"
#include "HepMC/IO_GenEvent.h"

using namespace Pythia8;


class DecayUserHooks : public UserHooks {

public:

  // Constructor can set helicity definition. Destructor does nothing.
  DecayUserHooks() {}
  ~DecayUserHooks() {}

  // Allow a veto for the process level, to gain access to decays.
  bool canVetoProcessLevel() {return true;}

  // Access the event after resonance decays.
  bool doVetoProcessLevel(Event& process) {
    
    bool topDecayVeto = false;
    
    // Identify decayed W+- bosons from top decay and Higgs decay products
    int Wp   = -1;
    int Wm   = -1;
    for (int i = 0; i < process.size(); ++i) {
      if (Wp != -1 && Wm != -1) break;     
      // Find the pdg Id of the mother
      int mother1_pdgId = process[process[i].mother1()].id();
      int mother2_pdgId = process[process[i].mother2()].id();
      if (process[i].id() == 24 && (mother1_pdgId == 6 || mother2_pdgId == 6)) {
        Wp = i;  
      }
      else if (process[i].id() == -24 && (mother1_pdgId == -6 || mother2_pdgId == -6)) {
        Wm = i;  
      }
    }
        
    // Check the W decay products
    bool Wp_lep = process[process[Wp].daughter1()].isLepton();
    bool Wm_lep = process[process[Wm].daughter1()].isLepton();

    if ( (Wp_lep && Wm_lep) || (!Wp_lep && !Wm_lep) ) topDecayVeto = true;
        
    return topDecayVeto;
  }  
  
};

int main(int argc, char* argv[]) {

  // Check that correct number of command-line arguments
  if (argc != 3) {
    cerr << " Unexpected number of command-line arguments. \n You are"
         << " expected to provide: CMND HEPMC.OUT\n"
         << " Program stopped! " << endl;
    return 1;
  }

  ifstream isb(argv[1]);
  if (!isb) {
    cerr << " Command file " << argv[1] << " was not found. \n"
         << " Program stopped! " << endl;
    return 1;
  }
  
  // Confirm that external files will be used for input and output.
  cout << "\n >>> PYTHIA settings will be read from file " << argv[1]
       << " <<< \n >>> HepMC events will be written to file "
       << argv[2] << " <<< \n" << endl;

  // Interface for conversion from Pythia8::Event to HepMC event.
  HepMC::Pythia8ToHepMC ToHepMC;

  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(argv[2], std::ios::out);

  // Generator. 
  Pythia pythia;

  auto decayUserHooks = make_shared<DecayUserHooks>();
  pythia.setUserHooksPtr(decayUserHooks);

  // Read in commands from external file.
  pythia.readFile(argv[1]);

  // Allow for possibility of a few faulty events.
  int nAbort  = 10;
  int iAbort  = 0;
  int nEvent  = pythia.mode("Main:numberOfEvents");
  
  // Initialize Les Houches Event File run. List initialization information.
  pythia.init();
  
  // Begin event loop; generate until none left in input file.
  for (int iEvent = 0; ; ++iEvent) {
        
    if (nEvent > 0 && iEvent > nEvent-1) break;
    
    // Generate events, and check whether generation failed.
    if (!pythia.next()) {

      // If failure because reached end of file then exit event loop.
      if (pythia.info.atEndOfFile()) { std::cerr << "EOF reached. Exiting.\n"; break; }

      // First few failures write off as "acceptable" errors, then quit.
      if (++iAbort < nAbort) continue;
      std::cerr << "Exiting due to many errors\n";
      break;      
      
    }

    // Construct new empty HepMC event and fill it.
    // Units will be as chosen for HepMC build, but can be changed
    // by arguments, e.g. GenEvt( HepMC::Units::GEV, HepMC::Units::MM)
    HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    ToHepMC.fill_next_event( pythia, hepmcevt );
    
    // Write the HepMC event to file. Done with it.
    ascii_io << hepmcevt;
    delete hepmcevt;

  // End of event loop.
  }

  // Give statistics. Print histogram.
  pythia.stat();
    
  // Done.
  return 0;
}
