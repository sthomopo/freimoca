#!/usr/bin/env python

# The following is needed for stripping the weight names if they ever appear
# and for converting the yoda file to root
from array import array
import ROOT as rt
import yoda, sys

fName = sys.argv[1]
yodaAOs = yoda.read(fName)
rtFile = rt.TFile(fName[:fName.find('.yoda')] + '.root', 'recreate')
for name in yodaAOs:
	yodaAO = yodaAOs[name];  rtAO = None
	if 'Histo1D' in str(yodaAO):
		rtAO = rt.TH1D(name, '', yodaAO.numBins(), array('d', yodaAO.xEdges()))
		rtAO.Sumw2(); rtErrs = rtAO.GetSumw2()
		for i in range(rtAO.GetNbinsX()):
			rtAO.SetBinContent(i + 1, yodaAO.bin(i).sumW())
			rtErrs.AddAt(yodaAO.bin(i).sumW2(), i+1)
	elif 'Scatter2D' in str(yodaAO):
		rtAO = rt.TGraphAsymmErrors(yodaAO.numPoints())
		for i in range(yodaAO.numPoints()):
			x = yodaAO.point(i).x(); y = yodaAO.point(i).y()
			xLo, xHi = yodaAO.point(i).xErrs()
			yLo, yHi = yodaAO.point(i).yErrs()
			rtAO.SetPoint(i, x, y)
			rtAO.SetPointError(i, xLo, xHi, yLo, yHi)
	else:
		continue
	# Strip directory name
	name = name.split("/")[-1]
	# Strip weights
	weight="[_muR010000E+01_muF010000E+01_]"
	if name.endswith(weight):
		name = name[:-len(weight)]
	rtAO.Write(name)
rtFile.Close()
