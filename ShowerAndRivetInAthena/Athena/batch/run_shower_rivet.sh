#!/bin/zsh

mode=$1
CODEDIR=$2
INPUTFILE=$3
OUTPUTDIR=$4
DOSMEAR=$5
ANALYSIS=$6
SAMPLE=$7

# Function to run the shower routine (shower.py)
# It assumes that the INPUTFILE is a path to a single file
run_shower() {

  # A temporary directory is made where all input file and scripts will be stored
  TMPDIR=$(mktemp -d)
  echo $TMPDIR
  cd $TMPDIR
     
  # setup atlas environment  
  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase  
  . ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

  # asetup
  . /afs/cern.ch/atlas/software/dist/AtlasSetup/scripts/asetup.sh 19.2.5.19,here
               
  # Copy the input file to the temporary directory 
  cp $(cat $INPUTFILE) .
  filename=$(ls *.events.tar.gz) 
  tar zxf $filename && rm $filename
  basefilename=$(echo $filename | sed 's|.events.tar.gz||')
  mv $basefilename.events $basefilename.000001.events # since the transform is looking for $Mass.*.ev*ts

  echo "We are going to use: $basefilename.000001.events"
  if [ -f $basefilename.000001.events ] ; then
    echo "OK: $basefilename.000001.events file found"
  fi
  
  # Copy the Generate_tf command and the shower jO
  cp $CODEDIR/command.txt $CODEDIR/shower.py $TMPDIR/
  
  # update shower.py with corect Operator and Mass values
  sed -i "s/INPUTFILENAME/$basefilename/" shower.py
  
  echo "----------"
  cat shower.py
  echo "----------"
  cat command.txt
  echo "----------"
  
  # run the shower
  source command.txt
  
  if [ ! -f EVNT.root ] ; then
    echo "ERROR: Problem running the shower. EVNT file not generated. Aborting"
    exit 2
  fi
  
  grep -q "successful run" log.generate
  if [[ "$?" == "1" ]] ; then
    echo "ERROR: Not successful run. Aborting"
    exit 3
  fi
  
  if [ ! -d $OUTPUTDIR ] ; then
    mkdir $OUTPUTDIR
  fi
  
  echo "Copy to $OUTPUTDIR"
  cp EVNT.root $OUTPUTDIR/EVNT.$basefilename.root

  # clean up
  echo "Cleaning up..." 
  cd ..
  rm -rf $TMPDIR
  echo "Leaving run_shower..."
}

# Function to run the rivet routine 
# It assumes that the INPUTFILE is a file containing paths to multiple EVNT files
run_rivet() {
  
  ./runRivet.sh $INPUTFILE $CODEDIR/../../RivetRoutines/$ANALYSIS $OUTPUTDIR $DOSMEAR $SAMPLE
   
}
  
   
# Main script
if [[ "$mode" == "1" ]] ; then  
  run_shower
elif [[ "$mode" == "2" ]] ; then  
  run_rivet
elif [[ "$mode" == "3" ]] ; then  
  run_shower
  run_rivet
fi

exit 0
   
