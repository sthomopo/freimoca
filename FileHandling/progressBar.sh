progress() {
  done=$1
  total=$2
  n=$((10*done/total))
  printf "|%."$n"s%*c|\r" "----------" "$((10-n))"
}
