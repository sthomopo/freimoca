#!/usr/bin/env python3

import os, sys, getpass
from argparse import ArgumentParser

# Arguments
parser = ArgumentParser(usage=__doc__)
parser.add_argument("PROC", help="Process to submit")
parser.add_argument("-t", "--test",  action="store_true", default=False, dest="TEST",  help="only submit single job per sample")
parser.add_argument("-c", "--check", action="store_true", default=False, dest="CHECK", help="check jobs which have finished without submitting new jobs")
args = parser.parse_args()

# The function that submits the jobs
def submit(**kwargs):
	# Supported keys
	supportedKeys = ["proc", "mA", "mH", "widthA", "widthH", "tanb", "runMS"]
	# Iterate through arguments and find if there is any unknown 
	for key in kwargs:
		if key not in supportedKeys:
			print(f"ERROR: Unknown key: {key}")
			raise TypeError
	# Get list of expected arguments
	proc = kwargs["proc"]
	widthA = str(kwargs["widthA"])
	widthH = str(kwargs["widthH"])
	mA = str(kwargs["mA"])
	mH = str(kwargs["mH"])
	tanb = str(kwargs["tanb"])
	runMS = str(kwargs["runMS"])
	#print(f"Will set: proc = {proc} , mA = {mA:4d} , mH = {mH:4d} , widthA = {widthA:.3f} , widthH = {widthH:.3f} , MS = {runMS}")
	# Output directory
	outputDir=f"/nfs/dust/atlas/user/{getpass.getuser()}/LHE"	
	currentDir=os.getcwd()
	#jobName=f"{proc}_{mA}_{mH}_nw" if widthA == "0.001" else f"{proc}_{mA}_{mH}_lw"
	jobName=f"{proc}_{mA}_{mH}_tanb{tanb}
	# Prepare submission file
	os.system("cp sub sub.sh")
	with open("sub.sh", "r+") as file:
		filedata=file.read()
		filedata = filedata.replace("SUBMITDIR", currentDir)
		filedata = filedata.replace("PROCESS", proc)
		filedata = filedata.replace("MASS_A", mA)
		filedata = filedata.replace("MASS_H", mH)
		filedata = filedata.replace("WIDTH_A", widthA)
		filedata = filedata.replace("WIDTH_H", widthH)
        filedata = filedata.replace("TANB", tanb)
		filedata = filedata.replace("RUNMS", runMS)
		filedata = filedata.replace("OUTDIR", outputDir)
		filedata = filedata.replace("JOBNAME", jobName)
		# Replace file contents
		file.seek(0)
		file.write(filedata)
	
	# Expected output
	MStag="_decayed" if runMS else ""
	output1 = f"{outputDir}/{jobName}/Events/run_01/{jobName}_1.events.tar.gz"
	output2 = f"{outputDir}/{jobName}/Events/run_02/{jobName}_2.events.tar.gz"
	
	# Submit job
	if proc == "AZH_lltt":
		outputExists = os.path.exists(output1) and os.path.exists(output2)
	else:
		outputExists = os.path.exists(output1)
	if not outputExists:
		if not args.CHECK: 
            if not os.path.exists(f"{outputDir}/{jobName}"):
                os.system(f"mkdir -p {outputDir}/{jobName}")
			os.system("condor_submit sub.sh")
	# clean up
	if args.CHECK:
		if not outputExists:
			print(f"Sample: {jobName} missing")
		else:
			print(f"Sample {jobName} done")	


# Main function
def main():
	# Define masses to generate (mA, mH) 	
	if args.PROC == "AZH_lltt":
		mAHvalues = [(500,400),
				 	(550,400),  (550,450),
				 	(600,400),  (600,450),  (600,500),
				 	(650,400),  (650,450),  (650,500),   (650,550),
				 	(700,400),  (700,450),  (700,500),   (700,550),   (700,600),
				 	(750,400),  (750,450),  (750,500),   (750,550),   (750,600),
				 	(800,400),  (800,450),  (800,500),   (800,550),   (800,600),
				 	(900,400),  (900,450),  (900,500),   (900,600),   (900,700),
				 	(1000,400), (1000,600), (1000,900),
				 	(1200,400), (1200,600), (1200,900),  (1200,1100),
				 	(1500,400), (1500,700), (1500,1000), (1500,1200), (1500,1400),
				 	(1700,400), (1700,800), (1700,1200), (1700,1600),
				 	(2000,400), (2000,800), (2000,1200), (2000,1800)]
	elif args.PROC == "AZH_vvbb":
		mAHvalues = [(400,200),  (400,300),
					 (500,200),  (500,300),  (500,400),
				 	 (600,200),  (600,300),  (600, 400), (600, 500),
					 (700,200),  (700,300),  (700,400),  (700, 500),
				 	 (800,200),  (800,300),  (800,400),  (800,500),  (800,600),
					 (900,200),  (900,400),  (900,600),
				 	 (1000,200), (1000,400), (1000,600), (1000,800),
				 	 (1200,200), (1200,400), (1200,600), (1200,900), (1200,1100)]
	elif args.PROC == "bbMET_IDM":
		mAHvalues = [(400,200),
					(600,200),  (600,400),
				 	(800,200),  (800,400),  (800,600),
				 	(1000,200), (1000,400), (1000,600), (1000, 800)]
	else:
		print(f"Unknown process: {args.PROC}")
		sys.exit(1)
	# Define A and H widths
	widthA = "Auto" #1E-3
	widthH = "Auto" #1E-3	
	for mA, mH in mAHvalues:
		# Call submit function with the arguments we want
		submit(proc=args.PROC, mA=mA, mH=mH, widthA=widthA, widthH=widthH, runMS=True, tanb=1.0)
		# Only submit 1 job if testing
		if args.TEST:
			break

# If executed as a script run main function
if __name__ == "__main__":
    main()




